@extends('layouts.layout')

@section('login')

    <form  method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}" class="form-signin">
        @csrf
        <div class="text-center mb-4">
            <img class="mb-4" src="{{ asset('img/LMN-transparent.png') }}" alt="" width="200">
            {{-- <h1 class="h3 mb-3 font-weight-normal">LMN Technologies</h1> --}}
            <p>For administrator only and other member of the company. Not authorize person is not allowed.</p>
        </div>

        <div class="form-label-group">
            <input type="email" id="email" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}" placeholder="Email address" required autofocus>
            <label for="email">Email address</label>
            @if ($errors->has('email'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-label-group">
            <input type="password" id="password" name="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Password" required>
            <label for="password">Password</label>
            @if ($errors->has('password'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>
        <!-- <div class="form-group row">
            <div class="col-md-6 offset-md-4">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                    <label class="form-check-label" for="remember">
                        {{ __('Remember Me') }}
                    </label>
                </div>
            </div>
        </div> -->
        
        <button type="submit" class="btn btn-lg btn-violet btn-block">{{ __('Login') }}</button>
        <p class="mt-5 mb-3 text-muted text-center"><a href="/">Back to Home</a></p>

    </form>

@endsection