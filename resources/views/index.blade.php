@extends('layouts.layout')

@section('content')

<a href="#top-page" class="js-scroll-trigger">
    <div id="btn-back-to-top" class="text-center">
        <i class="mdi mdi-chevron-up" aria-hidden="true"></i>
    </div>
</a>

<section class="homebanner">
    <div class="bg-img1 size1 overlay1" style="background-image: url('img/background/christopher-gower-unsplash.png');">
        <div class="container txt-center text-white header-content">
            <div class="wsize1" style="padding-top: 150px;">
                <p class="txt-center l1-txt1">
                   INNOVATE THE FUTURE WITH YOU
                   {{-- Innovate the future with you --}}
                </p>
                
                <p class="txt-center s1-txt1">
                    We love to work with you, and let's change for the better community.
                </p>
                <p>
                    <i class="mdi mdi-gamepad-variant"></i> Game Development
                </p>
                <p>
                    <i class="mdi mdi-web"></i> Website Development
                </p>
                <p>
                    <i class="mdi mdi-cellphone-link"></i> Mobile and Software Development
                </p>
                <p>
                    <i class="mdi mdi-internet-explorer"></i>  Social Media/Interet Marketing
                </p>
                {{-- <p>
                    <i class="mdi mdi-lead-pencil"></i> Ghraphic Design & Advertising
                </p> --}}
                <p>
                    <i class="mdi mdi-code-braces"></i> IT Training & Consultation
                </p>
            </div>
        </div>
    </div>
    {{-- <div id="demo" class="carousel slide carousel-fade" data-ride="carousel">
      <ul class="carousel-indicators">
        <li data-target="#demo" data-slide-to="0" class="active"></li>
        <li data-target="#demo" data-slide-to="1"></li>
        <li data-target="#demo" data-slide-to="2"></li>
      </ul>
      <div class="carousel-inner">
        <div class="carousel-item active">
            <div class="bg-gradient-MidnightCity">
            </div>
            <img src="{{ asset('img/background/christopher-gower-unsplash.png') }}" alt="Los Angeles" width="1100" height="500">
            <div class="carousel-caption text-left animated fadeInRight">
                <h1>INNOVATE THE FUTURE WITH YOU</h1>
                <p>We love to work with you!</p>
            </div>
        </div>
        <div class="carousel-item">
            <div class="bg-gradient-MidnightCity">
            </div>
          <img src="{{ asset('img/background/img001.jpg') }}" alt="Chicago" width="1100" height="500">
          <div class="carousel-caption animated zoomIn">
            <h3>VIRTUAL REALITY</h3>
            <p>Experience the virtual world.</p>
          </div>
        </div>
        <div class="carousel-item">
            <div class="bg-gradient-MidnightCity">
            </div>
          <img src="{{ asset('img/background/img003.jpg') }}" alt="New York" width="1100" height="500">
          <div class="carousel-caption text-right animated fadeInLeft">
            <h3>LET'S CHANGE FOR THE BETTER COMMUNITY</h3>
            <p>We do our best to make you the best.</p>
          </div>
        </div>
      </div>
      <a class="carousel-control-prev" href="#demo" data-slide="prev">
        <span class="carousel-control-prev-icon"></span>
      </a>
      <a class="carousel-control-next" href="#demo" data-slide="next">
        <span class="carousel-control-next-icon"></span>
      </a>
    </div> --}}
</section>
<!--Banner Section-->

<!--Banner Section-->

<!--Who we are Section-->
    <section id="info-section" class="info-section">
        <div class="bg-overlay">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12 text-info who-are-we">
                        <!--Section Heading-->
                        <div class="heading">
                            <h1 class="wwa">Who we are?</h1>
                            <span style="color: #353535;">W</span>
                        </div>

                        <div class="content">
                            <p class="">A startup company founded in the Bicol region that caters to the local and international (onsite and online) IT communities.</p>
                            <p class="">LMN Technologies advocates the programs of the Department of Information and Communications Technology as well as pursuing the international initiatives in the field of Information Technology and Computing through collaboration and innovation.</p>
                        </div>
                    </div>

                    <!--Youtube Video Thumb-->
                    <div class="col-md-6 col-md-offset-1 col-sm-8 col-sm-offset-0 col-xs-12 who-are-we-img">
                        <img class="img-responsive" src="{{ asset('img/background/LMN.png') }}" alt="image" style="width: 100%;">
                    </div>
                </div>
            </div>
        </div>
    </section>

<!--Team Section-->
    <section id="team-section" class="team-section">
        <div class="col-md-12 col-sm-12 col-xs-12 text-center team-header">
            <div class="heading dark-bg text-center our-team text-white">
                <h1>OUR TEAMS</h1>
            </div>
            <div class="content our-team-content">
                <p style="line-height: 10pt;">LMN Technologies is proudly being handled by the following experts:</p><br>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <!-- Team member -->
                <div class="col-xs-12 col-sm-6 col-md-4 team-content">
                    <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                        <div class="mainflip">
                            <div class="frontside">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <p><img class=" img-fluid" src="{{ asset('img/teams/april.png') }}" alt="card image"></p>
                                        <h4 class="card-title">APRIL GIANAN</h4>
                                        <p class="card-text">Founder & CEO</p>
                                        {{-- <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i></a> --}}
                                    </div>
                                </div>
                            </div>
                            {{-- <div class="backside">
                                <div class="card">
                                    <div class="card-body text-center mt-4">
                                        <h4 class="card-title">APRIL GIANAN</h4>
                                        <p class="card-text">This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.</p>
                                        <ul class="list-inline">
                                            <li class="list-inline-item">
                                                <a class="social-icon text-xs-center" target="_blank" href="#">
                                                    <i class="mdi mdi-facebook facebook-color" aria-hidden="true"></i>
                                                </a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a class="social-icon text-xs-center" target="_blank" href="#">
                                                    <i class="mdi mdi-twitter twitter-color" aria-hidden="true"></i>
                                                </a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a class="social-icon text-xs-center" target="_blank" href="#">
                                                    <i class="mdi mdi-gmail gmail-color" aria-hidden="true"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div> --}}
                        </div>
                    </div>
                </div>
                <!-- ./Team member -->
                <!-- Team member -->
                <div class="col-xs-12 col-sm-6 col-md-4 team-content">
                    <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                        <div class="mainflip">
                            <div class="frontside">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <p><img class=" img-fluid" src="{{ asset('img/teams/jemarwen.jpg') }}" alt="card image"></p>
                                        <h4 class="card-title">JEMARWEN B. BALDON</h4>
                                        <p class="card-text">Founder & CEO</p>
                                        {{-- <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i></a> --}}
                                    </div>
                                </div>
                            </div>
                            {{-- <div class="backside">
                                <div class="card">
                                    <div class="card-body text-center mt-4">
                                        <h4 class="card-title">JEMARWEN B. BALDON</h4>
                                        <p class="card-text">This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.</p>
                                        <ul class="list-inline">
                                            <li class="list-inline-item">
                                                <a class="social-icon text-xs-center" target="_blank" href="#">
                                                    <i class="mdi mdi-facebook facebook-color" aria-hidden="true"></i>
                                                </a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a class="social-icon text-xs-center" target="_blank" href="#">
                                                    <i class="mdi mdi-twitter twitter-color" aria-hidden="true"></i>
                                                </a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a class="social-icon text-xs-center" target="_blank" href="#">
                                                    <i class="mdi mdi-gmail gmail-color" aria-hidden="true"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div> --}}
                        </div>
                    </div>
                </div>
                <!-- ./Team member -->
                <!-- Team member -->
                <div class="col-xs-12 col-sm-6 col-md-4 team-content">
                    <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                        <div class="mainflip">
                            <div class="frontside">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <p><img class=" img-fluid" src="{{ asset('img/teams/bryanbelardo.jpg') }}" alt="card image"></p>
                                        <h4 class="card-title">BRYAN BELARDO</h4>
                                        <p class="card-text">Co-Founder & CTO</p>
                                        {{-- <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i></a> --}}
                                    </div>
                                </div>
                            </div>
                            {{-- <div class="backside">
                                <div class="card">
                                    <div class="card-body text-center mt-4">
                                        <h4 class="card-title">ARCHIE LASALITA</h4>
                                        <p class="card-text">This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.</p>
                                        <ul class="list-inline">
                                            <li class="list-inline-item">
                                                <a class="social-icon text-xs-center" target="_blank" href="#">
                                                    <i class="mdi mdi-facebook facebook-color" aria-hidden="true"></i>
                                                </a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a class="social-icon text-xs-center" target="_blank" href="#">
                                                    <i class="mdi mdi-twitter twitter-color" aria-hidden="true"></i>
                                                </a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a class="social-icon text-xs-center" target="_blank" href="#">
                                                    <i class="mdi mdi-gmail gmail-color" aria-hidden="true"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div> --}}
                        </div>
                    </div>
                </div>
                <!-- ./Team member -->

            </div>
        </div>
    </section>


<!--Services Section-->
    <section id="services-section" class="services-section">
        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
            <div class="heading dark-bg text-center animated fadedown our-services">
                <h1>Our Services</h1>
            </div>
            <div class="content services-content">
                <!-- <p style="line-height: 10pt;">Some text information Some text information Some text information Some text information .</p> -->
                <p style="line-height: 10pt;">Web services & Virtual services</p>
            </div>
        </div>
        <div class="container lmn-card-container">
            <div class="lmn-card col-lg-4 text-center">
                <div class="lmn-card-header">
                    <!-- <i class="fa fa-plane circle-icon" aria-hidden="true"></i> -->
                    <i class="mdi mdi-gamepad-variant circle-icon" aria-hidden="true"></i>
                    <!-- <img class="img-responsive circle-icon" src="{{ asset('img/services/bryan.jpg') }}" alt="image" width="100%"> -->
                </div>
                <div class="lmn-card-body">
                    <h3>Game Development</h3>
                    {{-- <p>body text body text body text body text body text body text body text body text body text body text body text body text body text body text body text body text body text </p> --}}
                </div>
                <div class="lmn-card-footer">
                     <!-- <a href="" class="btn btn-SeeMore">View Details <i class="fa fa-eye" aria-hidden="true"></i></a>  -->
                </div>
            </div>
            <div class="lmn-card col-lg-4 text-center">
                <div class="lmn-card-header">
                    <!-- <i class="fa fa-plane circle-icon" aria-hidden="true"></i> -->
                    <i class="mdi mdi-web circle-icon" aria-hidden="true"></i>
                    <!-- <img class="img-responsive circle-icon" src="{{ asset('img/services/bryan.jpg') }}" alt="image" width="100%"> -->
                </div>
                <div class="lmn-card-body">
                    <h3>Web Development</h3>
                    {{-- <p>body text body text body text body text body text body text body text body text body text body text body text body text body text body text body text body text body text </p> --}}
                </div>
                <div class="lmn-card-footer">
                     <!-- <a href="" class="btn btn-SeeMore">View Details <i class="fa fa-eye" aria-hidden="true"></i></a>  -->
                </div>
            </div>
            <div class="lmn-card col-lg-4 text-center">
                <div class="lmn-card-header">
                    <i class="mdi mdi-database circle-icon" aria-hidden="true"></i>
                </div>
                <div class="lmn-card-body">
                    <h3>Database Management System</h3>
                    {{-- <p>body text body text body text body text body text body text body text body text body text body text body text body text body text body text body text body text body text </p> --}}
                </div>
                <div class="lmn-card-footer">
                </div>
            </div>
            <div class="lmn-card col-lg-4 text-center">
                <div class="lmn-card-header">
                    <!-- <i class="fa fa-plane circle-icon" aria-hidden="true"></i> -->
                    <i class="mdi mdi-cellphone-link circle-icon" aria-hidden="true"></i>
                    <!-- <img class="img-responsive circle-icon" src="{{ asset('img/services/bryan.jpg') }}" alt="image" width="100%"> -->
                </div>
                <div class="lmn-card-body">
                    <h3>Mobile & Software Development</h3>
                    {{-- <p>body text body text body text body text body text body text body text body text body text body text body text body text body text body text body text body text body text </p> --}}
                </div>
                <div class="lmn-card-footer">
                     <!-- <a href="" class="btn btn-SeeMore">View Details <i class="fa fa-eye" aria-hidden="true"></i></a>  -->
                </div>
            </div>
            <div class="lmn-card col-lg-4 text-center">
                <div class="lmn-card-header">
                    <!-- <i class="fa fa-plane circle-icon" aria-hidden="true"></i> -->
                    <i class="mdi mdi-internet-explorer circle-icon" aria-hidden="true"></i>
                    <!-- <img class="img-responsive circle-icon" src="{{ asset('img/services/bryan.jpg') }}" alt="image" width="100%"> -->
                </div>
                <div class="lmn-card-body">
                    <h3>Social Media/Internet Marketing</h3>
                    {{-- <p>body text body text body text body text body text body text body text body text body text body text body text body text body text body text body text body text body text </p> --}}
                </div>
                <div class="lmn-card-footer">
                     <!-- <a href="" class="btn btn-SeeMore">View Details <i class="fa fa-eye" aria-hidden="true"></i></a>  -->
                </div>
            </div>
           {{--  <div class="lmn-card col-lg-4 text-center">
                <div class="lmn-card-header">
                    <i class="mdi mdi-lead-pencil circle-icon" aria-hidden="true"></i>
                </div>
                <div class="lmn-card-body">
                    <h3>Graphic Design & Advertising</h3>
                    <p>body text body text body text body text body text body text body text body text body text body text body text body text body text body text body text body text body text </p>
                </div>
                <div class="lmn-card-footer">
                     <a href="" class="btn btn-SeeMore">View Details <i class="fa fa-eye" aria-hidden="true"></i></a> 
                </div>
            </div> --}}
            <div class="lmn-card col-lg-4 text-center">
                <div class="lmn-card-header">
                    <!-- <i class="fa fa-plane circle-icon" aria-hidden="true"></i> -->
                    <i class="mdi mdi-code-braces circle-icon" aria-hidden="true"></i>
                    <!-- <img class="img-responsive circle-icon" src="{{ asset('img/services/bryan.jpg') }}" alt="image" width="100%"> -->
                </div>
                <div class="lmn-card-body">
                    <h3>IT Training & IT Consultation</h3>
                    {{-- <p>body text body text body text body text body text body text body text body text body text body text body text body text body text body text body text body text body text </p> --}}
                </div>
                <div class="lmn-card-footer">
                     <!-- <a href="" class="btn btn-SeeMore">View Details <i class="fa fa-eye" aria-hidden="true"></i></a>  -->
                </div>
            </div>


        </div>
    </section>

    <!-- <section id="pricing-section" class="pricing-table">
        <div class="container">
            <div class="block-heading">
              <h2>Pricing Table</h2>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc quam urna, dignissim nec auctor in, mattis vitae leo.</p>
            </div>
            <div class="row justify-content-md-center">
                <div class="col-md-5 col-lg-4">
                    <div class="item">
                        <div class="heading">
                            <h3>BASIC</h3>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        <div class="features">
                            <h4><span class="feature">Full Support</span> : <span class="value">No</span></h4>
                            <h4><span class="feature">Duration</span> : <span class="value">30 Days</span></h4>
                            <h4><span class="feature">Storage</span> : <span class="value">10GB</span></h4>
                        </div>
                        <div class="price">
                            <h4>$25</h4>
                        </div>
                        <button class="btn btn-block btn-outline-primary" type="submit">BUY NOW</button>
                    </div>
                </div>
                <div class="col-md-5 col-lg-4">
                    <div class="item">
                        <div class="ribbon">Best Value</div>
                        <div class="heading">
                            <h3>PRO</h3>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        <div class="features">
                            <h4><span class="feature">Full Support</span> : <span class="value">Yes</span></h4>
                            <h4><span class="feature">Duration</span> : <span class="value">60 Days</span></h4>
                            <h4><span class="feature">Storage</span> : <span class="value">50GB</span></h4>
                        </div>
                        <div class="price">
                            <h4>$50</h4>
                        </div>
                        <button class="btn btn-block btn-primary" type="submit">BUY NOW</button>
                    </div>
                </div>
                <div class="col-md-5 col-lg-4">
                    <div class="item">
                        <div class="heading">
                            <h3>PREMIUM</h3>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        <div class="features">
                            <h4><span class="feature">Full Support</span> : <span class="value">Yes</span></h4>
                            <h4><span class="feature">Duration</span> : <span class="value">120 Days</span></h4>
                            <h4><span class="feature">Storage</span> : <span class="value">150GB</span></h4>
                        </div>
                        <div class="price">
                            <h4>$150</h4>
                        </div>
                        <button class="btn btn-block btn-outline-primary" type="submit">BUY NOW</button>
                    </div>
                </div>
            </div>
        </div>
    </section> -->

    <section id="contact-section" class="contact-section footer footer-bg">
      <div class="container">
        <div class="row">
          <div class="" style="width: 100%;">
            <h2>Contact us</h2>
            <label class="secondary-text">A startup company founded in the Bicol region that caters to the local and international (onsite and online) IT communities. </label>
            <form id="frm-add-message" method="post" onsubmit="return false;">
                @csrf
                <div class="row">
                    <div class="col-lg-4 d-flex flex-column address-wrap">
                        <div class="single-contact-address d-flex flex-row">
                            <div class="icon">
                                <i class="mdi mdi-home"></i>
                            </div>
                            <div class="contact-details">
                                <h5>Rizal Street Legazpi, Albay</h5>
                                <p>
                                    Legazpi, Albay 4500
                                </p>
                            </div>
                        </div>
                        <div class="single-contact-address d-flex flex-row">
                            <div class="icon">
                                <i class="mdi mdi-phone"></i>
                            </div>
                            <div class="contact-details">
                                <h5>(052) 736 0037</h5>
                                <p>Mon to Fri 9am to 6 pm</p>
                            </div>
                        </div>
                        <div class="single-contact-address d-flex flex-row">
                            <div class="icon">
                                <i class="mdi mdi-email"></i>
                            </div>
                            <div class="contact-details">
                                <h5>lmntech18@gmail.com</h5>
                                <p>Send us your query anytime!</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                            <div class="form-group">
                                <input id="name" name="name" placeholder="Enter your name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your name'" class="common-input mb-20 form-control" required="" type="text">
                                <input id="email" name="email" placeholder="Enter email address" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter email address'" class="common-input mb-20 form-control" required="" type="email">
                                <input id="mobile_number" name="mobile_number" placeholder="Enter mobile #" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter mobile #'" class="common-input mb-20 form-control" required="" type="number">
                                <select class="form-control form-control-lg" id="subject" name="subject">
                                    @foreach ($subjects as $subject => $result)
                                        <option value="{{ $result->subjects }}">{{ $result->subjects }}</option>
                                    @endforeach
                                </select>
                            </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <textarea class="common-textarea form-control" name="message" id="message" placeholder="Enter Messege" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Messege'" required=""></textarea>
                        </div>
                        <div class="">
                            <div class="alert-msg" style="text-align: left;"></div>
                            <button class="btn btn-outline-primary btn-send-message" id="btn-send-message" style="float: right;">Send Message</button>
                        </div>
                    </div>
                </div>
            </form>
          </div>
        </div>
      </div>
    </section>
    
    <section class="mbr-section mbr-section-nopadding map-location" id="map1-5" data-rv-view="21">
        <div class="mbr-map">
            <iframe frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyA0Dx_boXQiwvdz8sJHoYeZNVTdoWONYkU&amp;q=place_id:ChIJp_ibfmgBoTMRelMUjedSUjA" allowfullscreen=""></iframe>
        </div>
    </section>
    <div class="sectionP40 copyright-footer">
        <div class="container">
            <p class="text-white copyright-text">
                &copy; Copyright 2018 <a class="js-scroll-trigger text-violet" href="#top-page">LMN Technologies</a>. All rights reserved. {{-- <a href="#terms">Terms of Use</a> | <a href="#privacy">Privacy Policy</a> --}}
            </p>
        </div>
    </div>

    <!-- Footer Section -->
@endsection