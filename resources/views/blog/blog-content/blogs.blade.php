{{-- <div class="card-blog-post">
    <div class="row">
        <div class="col-md-4">
            <img src="{{ asset('img/background/ewan-robertson-208022-unsplash.jpg') }}" alt="Image" style="" width="100%">
        </div>
        <div class="col-md-8">
            <div class="blog_title">
                <p>
                    <strong>Technologies from now on.</strong><br>
                    <label for="blog_title" class="small category"><strong>Category: </strong>Technologies</label>
                </p>
            </div>
            <div class="blog_description">
                <label class="small">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.…</label>
            </div>
            <div class="foot">
                <div class="blog-footer">
                    <div class="row">
                        <div class="col-md-8">
                            <label for="" class="small"><strong>Published: </strong>February 19, 2014</label>
                        </div>
                        <div class="col-md-4">
                            <a href="/blogs/1" class="btn btn-read btn-sm pull-right">Read More ...</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 --}}


<div class="row">
    @foreach ($blogs as $blog => $result)
        <div class="col-lg-6 col-sm-12 blog-item" id="blog{{ $result->id }}">
            <div class="card h-100 blog-card">
                <div class="blog-image">
                    <img src="{{ asset("/img/blog/post/$result->image") }}" class="item_loading_{{ $result->id }}" alt="{{ $result->title }}" style="">
                </div>
                <div class="blog-title">
                    <h5 class="txt_title_{{ $result->id }}" data-toggle="tooltip" data-placement="right" title="{{ $result->title }}">{{ $result->title }}</h5>
                    <div class="d-flex flex-row align-items-center text-gray">
                        <i class="mdi mdi-calendar icon-sm"></i>
                        <p class="mb-0 ml-1">
                          <small>{{ date('F d, Y', strtotime($result->created_at)) }}</small>
                        </p>
                    </div>
                    <div class="d-flex flex-row align-items-center text-gray">
                        <i class="mdi mdi-clock icon-sm"></i>
                        <p class="mb-0 ml-1">
                            <small>{{ $result->created_at->format('h:i:s A') }}</small>
                        </p>
                    </div>
                    <a href="{{url("blog/$result->id")}}" class="btn btn-read btn-sm pull-right">Read More ...</a>
                </div>
            </div>
        </div>
    @endforeach
</div>