<div class="card-blog-post">
	<div class="blog-title">
		<h3>{{ $blog->title }}</h3>
		{{-- <label for="blog-title" class="small category"><strong>Category: </strong>Technologies</label> --}}
		<label for="blog-title" class="small" style="display: -webkit-inline-box;">
			<strong>Published: </strong>
			<p style="padding-left: 5px;">{{ date('F d, Y', strtotime($blog->created_at)) .' at '. $blog->created_at->format('h:i:s A') }}</p>
		</label>
	</div>
	<div class="blog-img">
        <img src="{{ asset("/img/blog/post/$blog->image") }}" alt="Image" style="" width="100%">
<!-- 		<div class="footer">
			<i class="mdi mdi-heart" name="heart-icon"></i><label for="heart-icon" class="small">(10)</label>
			<i class="mdi mdi-comment" name="comment-icon"></i><label for="comment-icon" class="small">(11)</label>
		</div> -->
	</div>
	<p class="blog-content" style="margin-top: 20px;">
		<input type="hidden" value="{{ $blog->content }}" class="contenblog">
	</p>

	{{-- <hr>
	<div class="container mt-3">
	  <h2>Comments</h2>
	  <p>Media objects can also be nested (a media object inside a media object):</p><br>
	  <div class="media border p-3">
	    <img src="{{ asset('img/members/admin.jpg') }}" alt="John Doe" class="mr-3 mt-3 rounded-circle" style="width:60px;">
	    <div class="media-body">
	      <h4>Bryan Belardo <small><i>Posted on February 19, 2016</i></small></h4>
	      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
	      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
	      <div class="media p-3">
	        <img src="{{ asset('img/members/admin.jpg') }}" alt="Jane Doe" class="mr-3 mt-3 rounded-circle" style="width:45px;">
	        <div class="media-body">
	          <h4>Bryan Belardo <small><i>Posted on February 20 2016</i></small></h4>
	          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
	          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
	        </div>
	      </div>
	    </div>
	  </div>
	</div> --}}
</div>