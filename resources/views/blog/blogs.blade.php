@extends('layouts.layout')

@section('content')
    @if (isset($page) && $page == 'blogs')
        <section id="blogs-section" class="blogs-banner">
            <div class="bg-overlay">
            </div>
        </section>
        <section class="section-header">
            <div class="section_container container">
                <div class="header">
                    <div class="col-sm-8 col-sm-offset-2">
                        <h3 class="header_text">LMN TECHNOLOGIES SEO BLOGS</h3>
                        <p class="header_subtext">Home/Blog/</p>
                    </div>
                </div>
            </div>
        </section>
    @endif
    <section class="blogs">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-6 col-lg-8">
                    @if (isset($page) && $page == 'blogs')
                        @if (count($blogs) == 0)
                            {{-- empty --}}
                        @else
                            @include('blog.blog-content.blogs')
                        @endif
                    @elseif (isset($page) && $page == 'blog')
                        @include('blog.blog-content.blog')
                    @endif
                </div>
                {{-- division --}}
                <div class="col-6 col-lg-4">
                    <div class="popular">
                        <div class="card-popular-post">
                            <div class="header text-center">
                                <h6>Popular Post</h6>
                            </div>
                            <div class="content">
                                {{-- <a href="#post" class="postlink">
                                    <div class="post">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <img src="{{ asset('img/background/ewan-robertson-208022-unsplash.jpg') }}" alt="Image" style="" width="100%">
                                            </div>
                                            <div class="col-md-8">
                                                <div class="blog_title">
                                                    <p>
                                                        <strong>Technologies from now on.</strong><br>
                                                        <label for="blog_title" class="small category"><strong>Category: </strong>Technologies</label>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a> --}}
                            </div>
                        </div>
                    </div>

                    <div class="recently">
                        <div class="card-recently-post">
                            <div class="header text-center">
                                <h6>Recenlty Post</h6>
                            </div>
                            <div class="content">
                                {{-- <a href="#post" class="postlink">
                                    <div class="post">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <img src="{{ asset('img/background/ewan-robertson-208022-unsplash.jpg') }}" alt="Image" style="" width="100%">
                                            </div>
                                            <div class="col-md-8">
                                                <div class="blog_title">
                                                    <p>
                                                        <strong>Technologies from now on.</strong><br>
                                                        <label for="blog_title" class="small category"><strong>Category: </strong>Technologies</label>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection
