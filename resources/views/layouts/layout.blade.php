<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <title>LMN Technologies</title>
    <meta charset="UTF-8">
    <link rel="icon" href="{{ asset('/img/LMN logo.png') }}" type="image/x-icon">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, shrink-to-fit=no">
    <meta name="description" content="LMN Technologies is a blah blah blah.">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="{{ asset('/plugins/bootstrap-4.1.1-dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/plugins/mdi/css/materialdesignicons.min.css') }}" type="text/css" rel="stylesheet" />
    <link href="{{ asset('/plugins/animate.css-master/animate.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/plugins/croppie/croppie.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/layout.css') }}" type="text/css" rel="stylesheet" />
    <link href="{{ asset('/css/responsive.css') }}" type="text/css" rel="stylesheet" />

    @if (isset($page) && $page == 'index')
    	<link href="{{ asset('/css/index/index.css') }}" type="text/css" rel="stylesheet" />
    @elseif (isset($page) && $page == 'login')
    	<link href="{{ asset('/css/auth/login.css') }}" type="text/css" rel="stylesheet" />
    @elseif (isset($page) && $page == 'blogs')
    	<link href="{{ asset('/css/blog/blogs.css') }}" type="text/css" rel="stylesheet" />
    @elseif (isset($page) && $page == 'blog' || $page == 'blogs')
    	<link href="{{ asset('/css/blog/blogs.css') }}" type="text/css" rel="stylesheet" />
    	<link href="{{ asset('/css/blog/blog.css') }}" type="text/css" rel="stylesheet" />
    @elseif (isset($page) && $page == 'admin_blog' || $page == 'blog_update')
    	<link href="{{ asset('/css/admin/admin-blog.css') }}" type="text/css" rel="stylesheet" />
    	<link rel="stylesheet" href="{{ asset('/plugins/PurpleAdmin/vendors/css/vendor.bundle.base.css') }}">
    	<link rel="stylesheet" href="{{ asset('/plugins/PurpleAdmin/css/style.css') }}">
    @elseif (isset($page) && $page == 'admin_message')
    	<link href="{{ asset('/css/admin/admin-message.css') }}" type="text/css" rel="stylesheet" />
    	<link rel="stylesheet" href="{{ asset('/plugins/PurpleAdmin/vendors/css/vendor.bundle.base.css') }}">
    	<link rel="stylesheet" href="{{ asset('/plugins/PurpleAdmin/css/style.css') }}">
    @elseif (isset($page) && $page == 'admin_settings')
    	<link href="{{ asset('/css/admin/admin-settings.css') }}" type="text/css" rel="stylesheet" />
    	<link rel="stylesheet" href="{{ asset('/plugins/PurpleAdmin/vendors/css/vendor.bundle.base.css') }}">
    	<link rel="stylesheet" href="{{ asset('/plugins/PurpleAdmin/css/style.css') }}">
    @elseif (isset($page) && $page == 'pricing')
    	<link href="{{ asset('/css/pricing/pricing.css') }}" type="text/css" rel="stylesheet" />
    @endif

    <style>
        .carousel-inner img {
            width: 100%;
            height: 100%;
        }
        .ellip {
            display: block;
            height: 100%;
        }
        .ellip-line {
            display: inline-block;
            text-overflow: ellipsis;
            white-space: nowrap;
            word-wrap: normal;
            max-width: 100%;
        }
        .ellip,
        .ellip-line {
            position: relative;
            overflow: hidden;
        }
    </style>
</head>

<body id="top-page">
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-purple">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    @if (isset($page) && $page == 'index' || $page == 'blogs' || $page == 'blog' || $page == 'pricing')


        <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 box-shadow navbar-index">
	        <h5 class="my-0 mr-md-auto font-weight-normal text-link animated slideInLeft">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="mdi mdi-format-line-spacing"></i>
                </button>
				<a href="/#top-page" class="login-link js-scroll-trigger">
					<img class="header-logo animated rollIn lBlack" name="lBlack" src="{{ asset('/img/LMN logo.png') }}">
                    <img class="header-logo animated rollIn white" name="white" src="{{ asset('/img/LMN logo(white).png') }}">
					<p style="padding-top: 14px;">LMN TECHNOLOGIES</p>
				</a>
			</h5>
			@if (isset($page) && $page == 'blogs' || $page == 'blog' || $page == 'pricing')
                <nav class="my-2 my-md-0 mr-md-3 nav-collapse">
                    <div class="collapse-item" id="navbarResponsive">
                        <a class="p-2 text-link js-scroll-trigger" href="/#top-page">Home</a>
    		            <a class="p-2 text-link js-scroll-trigger" href="/#info-section">About</a>
    		            <a class="p-2 text-link js-scroll-trigger" href="/#team-section">Team</a>
    		            <a class="p-2 text-link js-scroll-trigger" href="/#services-section">Services</a>
    		            <!-- <a class="p-2 text-link js-scroll-trigger" href="/#pricing-section">Pricing</a> -->
    		            <a class="p-2 text-link js-scroll-trigger" href="/#contact-section">Contact</a>
    		            <a class="p-2 text-link" href="/blogs">Blog</a>
                    </div>
		        </nav>
	        @else
		        <nav class="my-2 my-md-0 mr-md-3 nav-collapse">
		            <div class="collapse-item" id="navbarResponsive">
                        <a class="p-2 text-link js-scroll-trigger" href="#top-page">Home</a>
                        <a class="p-2 text-link js-scroll-trigger" href="#info-section">About</a>
                        <a class="p-2 text-link js-scroll-trigger" href="#team-section">Team</a>
                        <a class="p-2 text-link js-scroll-trigger" href="#services-section">Services</a>
                        <!-- <a class="p-2 text-link js-scroll-trigger" href="#pricing-section">Pricing</a> -->
                        <a class="p-2 text-link js-scroll-trigger" href="#contact-section">Contact</a>
                        <a class="p-2 text-link" href="/blogs">Blog</a>      
                    </div>
		        </nav>
	        @endif
	    </div>
    @endif
    @if (isset($page) && $page == 'login')
        @yield('login')
    @else
        <div id="content">
            @yield('content')
        </div>
    @endif

    <!-- The Modal -->
    {{-- modal create blog --}}
    <div class="modal fade" id="create_blog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content bg-white pl-pr-20">
                <form id="frm_create_blog" action="{{ url('/blog/store') }}" method="post" enctype="multipart/form-data" onsubmit="return false;">
                    @csrf
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4>
								<div class="d-flex flex-row align-items-center">
		                            <i class="mdi mdi-blogger icon-md text-primary"></i>
		                            <p class="mb-0 ml-1">Create blog</p>
		                        </div>
							</h4>
                        <button type="button" class="close close_modal" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="drop-upload-area text-gray">
                                    <p>
                                        <i class="mdi mdi-image-area"></i>
                                        <span>Click or drag image here</span>
                                    </p>
                                </div>
                                <div class="image_area animated fadeIn" style="width:100%">
                                    <a href="javascript:;" id="select_image"><i class="mdi mdi mdi-upload icon-lg icon animated fadeIn"></i></a>
                                    <img src="" alt="" id="uploadedImage" name="uploadedImage" class="uploadedImage" width="100%">
                                </div>
                                <input type="file" name="img_blog" id="img_blog" accept="image/*"/>
                                <span class="text-danger image-valid">*The image may not be greater than 5mb</span>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="blogTitle">Title <span class="text-danger title-valid"> *Title is required</span></label>
                                    <input type="text" class="form-control" id="blogTitle" name="blogTitle" placeholder="Title">
                                </div>
                                <div class="form-group">
                                    <label for="blogContent">Content <span class="text-danger content-valid"> *Content is required</span></label>
                                    <textarea class="form-control" id="blogContent" name="blogContent" rows="8"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer nb-top">
                        <button type="submit" class="btn btn-gradient-primary btn-icon-text btn-save-blog">
                            <i class="mdi mdi-file-check btn-icon-prepend icon-save"></i> Save
                        </button>
                        <button type="button" class="btn btn-secondary btn-icon-text btn-cancel" data-dismiss="modal">
                            <i class="mdi mdi-close-circle btn-icon-prepend"></i> Cancel
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{-- modal view message --}}
    <div class="modal fade" id="view_message" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content bg-white">
                <form action="" method="post">
                    @csrf
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4>
							<div class="d-flex flex-row align-items-center">
	                            <i class="mdi mdi-email icon-md text-warning"></i>
	                            <p class="mb-0 ml-1">Message</p>
	                        </div>
						</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">
                        <p class="font-weight-bold">From:</p>
                        <p class="text-secondary message-info mb-2 sender_name"></p>
                        <p class="font-weight-bold">E-mail address:</p>
                        <p class="text-secondary message-info sender_email"></p>
                        <p class="font-weight-bold">Number:</p>
                        <p class="text-secondary message-info sender_number"></p>
                        <p class="font-weight-bold">Date & Time:</p>
                        <p class="text-secondary message-info sender_date_time"></p>
                        <p class="sender_message"></p>
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer nb-top">
                        <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{-- modal delete message --}}
    <div class="modal fade" id="delete_message" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content bg-white">
                <form id="frm-delete" method="post" action="">
                    @csrf
                    <input type="hidden" id="hdn-data-id" value="">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4>
							<div class="d-flex flex-row align-items-center">
	                            <i class="mdi mdi-delete-forever icon-lg text-danger"></i>
	                            <p class="mb-0 ml-1 msg-qsn"></p>
	                        </div>
						</h4>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer nb-top">
                        <button type="submit" class="btn btn-sm btn-gradient-danger btn-icon-text btn-yes-delete">
                            <i class="mdi mdi-delete-forever btn-icon-prepend icon-delete"></i> Yes
                        </button>
                        <button type="button" class="btn btn-sm btn-secondary btn-icon-text btn-cancel" data-dismiss="modal">
                            <i class="mdi mdi-close-circle btn-icon-prepend btn-cancel"></i> Cancel
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script src="{{ asset('/plugins/jquery-3.3.1/jquery-3.3.1.min.js') }}"></script>
    <script>
        window.jQuery || document.write('<script src="/plugins/jquery-3.3.1/jquery-3.3.1.min.js"><\/script>')
    </script>
    <script src="{{ asset('/plugins/bootstrap-4.1.1-dist/popper.min.js') }}"></script>
    <script src="{{ asset('/plugins/bootstrap-4.1.1-dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('/plugins/jquery-easing/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('/plugins/jQuery-slimScroll-master/jquery.slimscroll.js') }}"></script>
    <script src="{{ asset('/plugins/bootstrap-notify-master/bootstrap-notify.js') }}"></script>
    <script src="{{ asset('/plugins/bootstrap-notify-master/bootstrap-notify.min.js') }}"></script>
    <script src="{{ asset('/plugins/croppie/croppie.js') }}"></script>
    <script src="{{ asset('/plugins/jquery-ellipsis/jquery.ellipsis.min.js') }}"></script>
    <script src="{{ asset('/plugins/jquery-ellipsis/jquery.ellipsis.js') }}"></script>
    <script src="{{ asset('/js/classes/feed.js') }}"></script>

    <!-- Costum Js -->
    <script src="{{ asset('/js/onviewport-play-animation.js') }}"></script>
    <script src="{{ asset('/js/script.js') }}"></script>

    @if (isset($page) && $page == 'admin' )
    	<script src="{{ asset('/plugins/PurpleAdmin/js/off-canvas.js') }}"></script>
    	<script src="{{ asset('/plugins/PurpleAdmin/js/misc.js') }}"></script>
    	<script src="{{ asset('/plugins/PurpleAdmin/js/dashboard.js') }}"></script>
    @elseif (isset($page) && $page == 'index')
    	<script src="{{ asset('/js/index/index.js') }}"></script>
    @elseif (isset($page) && $page == 'admin_blog' || $page == 'blog_update')
    	<script src="{{ asset('/js/classes/blog.js') }}"></script>
    	<script src="{{ asset('/js/pages/blog.js') }}"></script>
    	<script src="{{ asset('/plugins/tinymce/tinymce.min.js') }}"></script>
    	<script src="{{ asset('/plugins/PurpleAdmin/js/off-canvas.js') }}"></script>
    	<script src="{{ asset('/plugins/PurpleAdmin/js/misc.js') }}"></script>
    @elseif (isset($page) && $page == 'admin_message')
    	<script src="{{ asset('/js/classes/message.js') }}"></script>
    	<script src="{{ asset('/js/pages/message.js') }}"></script>
    	<script src="{{ asset('/plugins/PurpleAdmin/js/off-canvas.js') }}"></script>
    <script src="{{ asset('/plugins/PurpleAdmin/js/misc.js') }}"></script>
    @elseif (isset($page) && $page == 'admin_settings')
    	<script src="{{ asset('/plugins/PurpleAdmin/js/off-canvas.js') }}"></script>
    	<script src="{{ asset('/plugins/PurpleAdmin/js/misc.js') }}"></script>
        <script src="{{ asset('/js/classes/settings.js') }}"></script>
        <script src="{{ asset('/js/pages/settings.js') }}"></script>
    @elseif (isset($page) && $page == 'blog')
        <script type="text/javascript">
            $(document).ready(function(){
                $('.blog-content').append($('.contenblog').val());
            });
        </script>
    @else
    @endif
    <input name="animation" type="hidden">
</body>

</html>