<div class="page-header" id="page_header">
    <h3 class="page-title">
    <span class="page-title-icon bg-gradient-primary text-white mr-2">
      <i class="mdi mdi-blogger"></i>
    </span>
    Blogs
  </h3>
    <nav aria-label="breadcrumb">
        <ul class="breadcrumb">
            <li>
                <a href="#Create blog" class="btn btn-gradient-primary pull-right create_blog" data-toggle="modal" data-target="#create_blog"> <i class="mdi mdi-plus icon-sm align-middle"></i> Create blog</a>
            </li>
        </ul>
    </nav>
</div>

<div class="row" id="all_blogs" style="display:block;">
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div class="container">
                    <div class="row">
                        <input type="hidden" class="blog_count" value="">
                        @if (count($blogs) == 0)
                            <div class="text-center no-blog" style="width: 100%">
                                <center>
                                    <h2><i class="mdi mdi-blogger text-gray"></i></h2>
                                    <h2 class="text-gray">You have no blog yet.</h2>
                                </center>
                            </div>
                        @else
                            <div class="new_blog"></div>
                            @foreach ($blogs as $blog => $result)
                                <div class="col-lg-4 col-sm-6 blog-item" id="blog{{ $result->id }}">
                                    <div class="card h-100 blog-card">
                                        <div class="blog-image">
                                            <img src="{{ asset("/img/blog/post/$result->image") }}" class="item_loading_{{ $result->id }}" alt="{{ $result->title }}" style="">
                                            <div class="ovrly"></div>
                                            <div id="blog_item_loader_{{ $result->id }}" class="blog_item_loader">
                                                <div class="preloader">
                                                    <div class="spinner-layer pl-white">
                                                        <div class="circle-clipper left">
                                                            <div class="circle"></div>
                                                        </div>
                                                        <div class="circle-clipper right">
                                                            <div class="circle"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="buttons">
                                                <a href="#delete_message" data-toggle="modal" class="btn btn-gradient-danger btn-rounded btn-icon animated fadeInUp btn-action btn-delete" data-id="{{ $result->id }}">
                                                    <i class="mdi mdi-delete-forever"></i>
                                                </a>
                                                <a href="#{{ $result->title }}" class="btn btn-gradient-success btn-rounded btn-icon animated fadeInUp btn-action btn-edit" data-id="{{ $result->id }}">
                                                    <i class="mdi mdi-pencil"></i>
                                                </a>
                                                <a href="#{{ $result->title }}" class="btn btn-gradient-primary btn-rounded btn-icon animated fadeInUp btn-action btn-view" data-id="{{ $result->id }}">
                                                    <i class="mdi mdi-eye"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="blog-title">
                                            <h5 class="txt_title_{{ $result->id }}" data-toggle="tooltip" data-placement="right" title="{{ $result->title }}">{{ $result->title }}</h5>
                                            <div class="d-flex flex-row align-items-center text-gray">
                                                <i class="mdi mdi-calendar icon-sm"></i>
                                                <p class="mb-0 ml-1">
                                                  <small class="date_created_{{ $result->id }}">{{ date('F d, Y', strtotime($result->created_at)) }}</small>
                                                </p>
                                            </div>
                                            <div class="d-flex flex-row align-items-center text-gray">
                                                <i class="mdi mdi-clock icon-sm"></i>
                                                <p class="mb-0 ml-1">
                                                    <small class="time_created_{{ $result->id }}">{{ $result->created_at->format('h:i:s A') }}</small>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- View blog --}}
<div class="row" id="blog_details">
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div class="container">
                    <div class="page-header" style="border-bottom: 1px solid #dadada;">
                        <h3>
                            <div class="d-flex flex-row align-items-center">
                                <i class="mdi mdi-blogger icon-sm text-primary"></i>
                                <p class="mb-0 ml-1">
                                    <a href="#blogs" id="back_to_blogs">Blog</a> <i class="mdi mdi-chevron-right icon-sm text-primary"></i> <span class="blog-title-span">Blog Title</span>
                                </p>
                            </div>
                        </h3>
                        <nav aria-label="breadcrumb">
                            <ul class="breadcrumb">
                                <li>
                                    <div class="d-flex flex-row align-items-center">
                                        <i class="mdi mdi-keyboard-backspace icon-sm text-primary"></i>
                                        <p class="mb-0 ml-1">
                                            <a href="#blogs" id="btn_back">Back</a>
                                        </p>
                                    </div>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    {{-- Blog details --}}
                    <div class="blog_details">
                        <h2 class="txt_title_blog">THIS IS BLOG TITLE</h2>
                        <div class="d-flex flex-row align-items-center">
                            <i class="mdi mdi-calendar icon-sm text-primary"></i>
                            <p class="mb-0 ml-1 text-gray txt_date">

                                <div class="d-flex flex-row align-items-center" style="padding-left: 10px;">
                                    <i class="mdi mdi-clock icon-sm text-success"></i>
                                    <p class="mb-0 ml-1 text-gray txt_time">

                                    </p>
                                </div>
                            </p>
                        </div>
                        <div class="image_blog">
                            <img src="" width="50%" alt="">
                        </div>
                        <div class="details">
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- Edit blog --}}
<div class="row" id="update_blog" style="display:none;">
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <form id="frm_update_blog" action="{{ url('/blog/update') }}" method="post" enctype="multipart/form-data" onsubmit="return false;">
                    @csrf
                    <div class="container">
                        <div class="page-header" style="border-bottom: 1px solid #dadada;">
                            <h3>
                                <div class="d-flex flex-row align-items-center">
                                    <i class="mdi mdi-blogger icon-sm text-primary"></i>
                                    <p class="mb-0 ml-1">
                                        <a href="#blogs" id="update_back_to_blogs">Blog</a> <i class="mdi mdi-chevron-right icon-sm text-primary"></i> <span class="">Update Blog</span>
                                    </p>
                                </div>
                            </h3>
                        </div>

                        <div class="blog_details">
                            <div class="form-group">
                                <label for="title">Title:</label>
                                <input type="text" class="form-control" id="Update_title" name="Update_title" placeholder="Enter title">
                                <input type="text" class="form-control" id="hdnID" name="hdnID" hidden>
                            </div>

                            <div class="image_blog">
                                <img src="" width="50%" id="update_image" alt=""> <br><br>
                                <button type="button" class="btn btn-gradient-primary btn-icon-text btn_new_image">
                                    <i class="mdi mdi-upload btn-icon-prepend"></i>
                                    Upload new image
                                </button>
                                <input type="file" name="new_image" id="new_image" hidden>
                                <input type="hidden" name="hdnimagename" id="hdnimagename" value="">
                            </div>

                            <div class="blog_content">
                                <div class="form-group">
                                    <label for="updateContent">Content:<span class="text-danger content-valid"> *Content is required</span></label>
                                    <textarea class="form-control" id="updateContent" name="updateContent" rows="8"></textarea>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-gradient-primary btn-icon-text btn_update_blog" data-id="">
                                <i class="mdi mdi-file-check btn-icon-prepend"></i>
                                Update
                            </button>
                            <button type="reset" class="btn btn-secondary btn-icon-text btn_cancel_blog">
                                <i class="mdi mdi-close-circle btn-icon-prepend"></i>
                                Cancel
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>