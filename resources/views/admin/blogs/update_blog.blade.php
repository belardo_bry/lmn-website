
<div class="row" id="blog_update">
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div class="container">
                    {{-- Blog details --}}

                    <form id="frm_update_blog" action="{{ url('/blog/update') }}" method="post" enctype="multipart/form-data" onsubmit="return false;">
                        @csrf
                        <div class="blog_details">
                            <div class="form-group">
                                <label for="blogTitle">Title</label>
                                <input type="text" class="form-control" id="blogTitle" name="blogTitle" placeholder="Title">
                            </div>
                            <div class="d-flex flex-row align-items-center">
                                <i class="mdi mdi-calendar icon-sm text-primary"></i>
                                <p class="mb-0 ml-1 text-gray txt_date">
                                    September 27, 1998 &nbsp;&nbsp;&nbsp;
                                    <div class="d-flex flex-row align-items-center">
                                        <i class="mdi mdi-clock icon-sm text-success"></i>
                                        <p class="mb-0 ml-1 text-gray txt_time">
                                            09:00 PM
                                        </p>
                                    </div>
                                </p>
                            </div>
                            <div class="image_blog">
                                <img src="/img/blog/blog_images/1537071571.jpg" width="50%" alt="">
                            </div>
                            <div class="details">
                                <div class="form-group">
                                    <label for="blogUpdate">Content <span class="text-danger content-valid"> *Content is required</span></label>
                                    <textarea class="form-control" id="blogUpdate" name="blogUpdate" rows="8"></textarea>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>