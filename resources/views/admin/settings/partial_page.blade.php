<div class="page-header">
  <h3 class="page-title">
    <span class="page-title-icon bg-gradient-primary text-white mr-2">
      <i class="mdi mdi-home"></i>
    </span>
    Settings
  </h3>
  <nav aria-label="breadcrumb">
    <ul class="breadcrumb">
      <li class="breadcrumb-item active" aria-current="page">
        <span></span>Settings
        <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
      </li>
    </ul>
  </nav>
</div>

<div class="row">
  <!-- slide show setting -->
{{--   <div class="col-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Slide Show</h4>
        <!-- <form class="forms-sample">
          <div class="form-group">
            <label for="exampleInputName1">Name</label>
            <input type="text" class="form-control" id="exampleInputName1" placeholder="Name">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail3">Email address</label>
            <input type="email" class="form-control" id="exampleInputEmail3" placeholder="Email">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword4">Password</label>
            <input type="password" class="form-control" id="exampleInputPassword4" placeholder="Password">
          </div>
          <div class="form-group">
            <label for="exampleSelectGender">Gender</label>
              <select class="form-control" id="exampleSelectGender">
                <option>Male</option>
                <option>Female</option>
              </select>
            </div>
          <div class="form-group">
            <label>File upload</label>
            <input type="file" name="img[]" class="file-upload-default">
            <div class="input-group col-xs-12">
              <input type="text" class="form-control file-upload-info" disabled placeholder="Upload Image">
              <span class="input-group-append">
                <button class="file-upload-browse btn btn-gradient-primary" type="button">Upload</button>
              </span>
            </div>
          </div>
          <div class="form-group">
            <label for="exampleInputCity1">City</label>
            <input type="text" class="form-control" id="exampleInputCity1" placeholder="Location">
          </div>
          <div class="form-group">
            <label for="exampleTextarea1">Textarea</label>
            <textarea class="form-control" id="exampleTextarea1" rows="4"></textarea>
          </div>
          <button type="submit" class="btn btn-gradient-primary mr-2">Submit</button>
          <button class="btn btn-light">Cancel</button>
        </form> -->
        <form class="sample-form" action="">
          <div class="row" style="margin-bottom: -30px;">
            <div class="col-md-4 stretch-card grid-margin">
              <div class="cardcard-img-holder">
                <img src="{{ asset('img/background/christopher-gower-unsplash.png') }}" class="carousel-img" alt="image name" width="100%" height="">
                <div class="form-group">
                  <label for="caption">Caption</label>
                  <input type="text" class="form-control" id="caption" placeholder="Caption title">
                </div>
                <div class="form-group caption">
                  <label for="sub-caption">Sub Caption</label>
                  <textarea class="form-control" id="sub-caption" rows="4" placeholder="Sub Caption"></textarea>
                </div>
              </div>
            </div>
            <div class="col-md-4 stretch-card grid-margin">
              <div class="card card-img-holder">
                <img src="{{ asset('img/background/img001.jpg') }}" class="carousel-img" alt="image name" width="100%" height="">
                <div class="form-group">
                  <label for="caption">Caption</label>
                  <input type="text" class="form-control" id="caption" placeholder="Caption title">
                </div>
                <div class="form-group caption">
                  <label for="sub-caption">Sub Caption</label>
                  <textarea class="form-control" id="sub-caption" rows="4" placeholder="Sub Caption"></textarea>
                </div>
              </div>
            </div>
            <div class="col-md-4 stretch-card grid-margin">
              <div class="card card-img-holder">
                <img src="{{ asset('img/background/img003.jpg') }}" class="carousel-img" alt="image name" width="100%" height="">
                <div class="form-group">
                  <label for="caption">Caption</label>
                  <input type="text" class="form-control" id="caption" placeholder="Caption title">
                </div>
                <div class="form-group caption">
                  <label for="sub-caption">Sub Caption</label>
                  <textarea class="form-control" id="sub-caption" rows="4" placeholder="Sub Caption"></textarea>
                </div>
              </div>
            </div>
          </div>
          <button type="submit" class="btn btn-gradient-primary mr-2">Save</button>
        </form>
      </div>
    </div>
  </div>
  <!-- Services settings -->
  <div class="col-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Services</h4>
        <p class="card-description">
          Basic form elements
        </p>
      </div>
    </div>
  </div>
  <!-- About settings -->
  <div class="col-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">About</h4>

        <div class="form-group caption">
          <textarea class="form-control" id="about-text" rows="10" placeholder="tell about the company"></textarea>
        </div>

        <button type="submit" class="btn btn-gradient-primary mr-2">Save</button>
      </div>
    </div>
  </div>
  <!-- Team settings -->
  <div class="col-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Teams</h4>
        <p class="card-description">
          Basic form elements
        </p>
      </div>
    </div>
  </div>
  <!-- Contact info settings -->
  <div class="col-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Contact info</h4>
        <p class="card-description">
          Basic form elements
        </p>
      </div>
    </div>
  </div> --}}
  <!-- username and password -->
  <div class="col-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">User Details</h4>
        <form id="frm_update_user" action="{{ url('/user/updateUser') }}" method="post" enctype="multipart/form-data" onsubmit="return false;">
          @csrf
          <div class="input-group mb-2 mr-sm-2">
            <input type="text" class="form-control" id="displayname" name="displayname" placeholder="Display Name" value="{{ Auth::user()->name }}">
          </div>
          <div class="input-group mb-2 mr-sm-2">
            <input type="email" class="form-control" id="email" name="email" name placeholder="Email" value="{{ Auth::user()->email }}">
          </div>

          {{-- <input type="password" class="form-control mb-2 mr-sm-2" id="newPassword" name="newPassword" placeholder="New Password" > --}}

          <button type="submit" class="btn btn-gradient-primary mb-2 btn_update_user">
            <i class="mdi mdi-file-check btn-icon-prepend"></i> Update
          </button>
        </form>
      </div>
    </div>
  </div>

  <div class="col-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Change Password</h4>
        <form id="frm_update_password" action="{{ url('/user/updatePassword') }}" method="post" enctype="multipart/form-data" onsubmit="return false;">
          @csrf
          <div class="input-group mb-2 mr-sm-2">
            <input type="password" class="form-control" id="newpassword" name="newpassword" name placeholder="New Password">
          </div>

          <div class="input-group mb-2 mr-sm-2">
            <input type="password" class="form-control mb-2 mr-sm-2" id="reenternewPassword" name="reenternewPassword" placeholder="Re-enter New Password" >
            <span class="invalid-feedback " id="invalidPass" role="alert">
                <strong>Password not match.</strong>
            </span>
          </div>

          <button type="submit" class="btn btn-gradient-primary mb-2 btn_update_user_password">
            <i class="mdi mdi-file-check btn-icon-prepend"></i> Update
          </button>
        </form>
      </div>
    </div>
  </div>

</div>