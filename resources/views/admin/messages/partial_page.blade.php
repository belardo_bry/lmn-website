{{--
<div class="page-header">
    <h3 class="page-title">
    <span class="page-title-icon bg-gradient-primary text-white mr-2">
      <i class="mdi mdi-message"></i>
    </span>
    Messages
  </h3>
    <nav aria-label="breadcrumb">
        <ul class="breadcrumb">
            <li class="breadcrumb-item active" aria-current="page">
                <span></span>Overview
                <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
            </li>
        </ul>
    </nav>
</div> --}}

<div class="row">
    <div class="col-12 grid-margin">
        <div class="card blog-post-card">
            <div class="card-body">

                <div class="page-header">
                    <h3 class="page-title">
            <span class="page-title-icon bg-gradient-primary text-white mr-2">
              <i class="mdi mdi-message"></i>
            </span>
            Messages
          </h3> {{--
                    <div class="btn-group">
                        <button type="button" class="btn btn-outline-primary btn-md dropdown-toggle" data-toggle="dropdown">Action </button>
                        <div class="dropdown-menu btn-action-dropdown">
                            @foreach ($subjects as $subject => $result)
                            <a class="dropdown-item action-dropdown" id="{{ $result->id }}">{{ $result->subjects }}</a> @endforeach
                            <a class="dropdown-item action-dropdown" id="">View all</a>
                        </div>
                    </div> --}}
                    <nav aria-label="breadcrumb">
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item active" aria-current="page">
                                <input type="text" hidden class="hdn-selected-row" value="0">
                                {{-- <button type="button" class="btn btn-gradient-danger btn-md btn-block btn-delete-all" id="btn-delete-all">Delete all</button> --}}
                            </li>
                        </ul>
                    </nav>
                </div>

                <div class="table-responsive slimscroll">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                {{-- <th style="width: 5px">
                                    <div class="custom-control custom-checkbox" style="margin-top: -30px;">
                                        <input type="checkbox" class="custom-control-input" id="checkall">
                                        <label class="custom-control-label" for="checkall"></label>
                                    </div>
                                </th> --}}
                                <th>Name</th>
                                <th>Message</th>
                                <th>subject</th>
                                <th>Mobile or Telephone #</th>
                                <th>Email address</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($messages as $message => $result)
                            <tr class="each-message" id="row{{ $result->id }}">
                                {{-- <td>
                                    <div class="custom-control custom-checkbox" style="margin-top: -30px;">
                                        <input type="checkbox" class="custom-control-input" id="msg{{ $result->id }}">
                                        <label class="custom-control-label" for="msg{{ $result->id }}"></label>
                                    </div>
                                </td> --}}
                                <td>
                                    <div class="d-flex flex-row align-items-center">
                                        {{-- <i class="mdi mdi-email-outline icon-md text-warning"></i> --}}
                                        <p class="text-name">
                                            {{ $result->name }}
                                        </p>
                                    </div>
                                </td>
                                <td>
                                    <p class="text-message">{{ $result->message }}</p>
                                </td>
                                <td>
                                    <label class="badge badge-gradient-success subject-budge">{{ $result->subject }}</label>
                                </td>
                                <td>
                                    {{ $result->mobile_number }}
                                </td>
                                <td>
                                    <p class="emails">{{ $result->email }}</p>
                                    <div class="message-action">
                                        <button type="button" id="{{ $result->id }}" class="btn btn-gradient-primary btn-rounded btn-icon animated fadeInRight btn-view-message">
                                            <i class="mdi mdi mdi-eye"></i>
                                        </button>
                                        <button type="button" id="{{ $result->id }}" class="btn btn-gradient-danger btn-rounded btn-icon animated fadeInRight btn-delete">
                                            <i class="mdi mdi mdi-delete-forever"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{--
                <ul class="nav nav-tabs-message">
                    <li><a href="#all" class="btn btn-outline-primary btn-sm active show" data-toggle="tab">All</a></li>
                    @foreach ($subjects as $subject => $result)
                    <li><a href="#tab{{ $result->id }}" class="btn btn-outline-primary btn-sm btn-tabs" data-toggle="tab">{{ $result->subjects }}</a></li>
                    @endforeach
                </ul>
                <div class="tab-content">
                    <div id="all" class="tab-pane animated fadeIn active show">
                        <div class="table-responsive slimscroll">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Message</th>
                                        <th>subject</th>
                                        <th>Mobile or Telephone #</th>
                                        <th>Email address</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($messages as $message => $result)
                                    <tr class="each-message" id="row{{ $result->id }}">
                                        <td>
                                            <div class="d-flex flex-row align-items-center">
                                                <i class="mdi mdi-email-outline icon-md text-warning"></i>
                                                <p class="mb-0 ml-1">
                                                    {{ $result->name }}
                                                </p>
                                            </div>
                                        </td>
                                        <td>
                                            <p class="text-message">{{ $result->message }}</p>
                                        </td>
                                        <td>
                                            <label class="badge badge-gradient-success subject-budge">{{ $result->subject }}</label>
                                        </td>
                                        <td>
                                            {{ $result->mobile_number }}
                                        </td>
                                        <td>
                                            <p class="emails">{{ $result->email }}</p>
                                            <div class="message-action">
                                                <button type="button" id="{{ $result->id }}" class="btn btn-gradient-primary btn-rounded btn-icon animated fadeInRight">
                                                    <i class="mdi mdi mdi-eye"></i>
                                                </button>
                                                <button type="button" id="{{ $result->id }}" class="btn btn-gradient-danger btn-rounded btn-icon animated fadeInRight">
                                                    <i class="mdi mdi mdi-delete-forever"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    @foreach ($subjects as $subject => $result)
                    <div id="tab{{ $result->id }}" class="tab-pane animated fadeIn">
                        <div class="table-responsive slimscroll">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Message</th>
                                        <th>Subject</th>
                                        <th>Mobile or Telephone #</th>
                                        <th>Email address</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($messages as $message => $messagresult) @if ($result->subjects == $messagresult->subject)
                                    <tr class="each-message" id="row{{ $messagresult->id }}">
                                        <td>
                                            <div class="d-flex flex-row align-items-center">
                                                <i class="mdi mdi-email-outline icon-md text-warning"></i>
                                                <p class="mb-0 ml-1">
                                                    {{ $messagresult->name }}
                                                </p>
                                            </div>
                                        </td>
                                        <td>
                                            <p class="text-message">{{ $messagresult->message }}</p>
                                        </td>
                                        <td>
                                            <label class="badge badge-gradient-success subject-budge">{{ $messagresult->subject }}</label>
                                        </td>
                                        <td>
                                            {{ $messagresult->mobile_number }}
                                        </td>
                                        <td>
                                            <p class="emails">{{ $messagresult->email }}</p>
                                            <div class="message-action">
                                                <button type="button" id="{{ $messagresult->id }}" class="btn btn-gradient-primary btn-rounded btn-icon animated fadeInRight">
                                                    <i class="mdi mdi mdi-eye"></i>
                                                </button>
                                                <button type="button" id="{{ $messagresult->id }}" class="btn btn-gradient-danger btn-rounded btn-icon animated fadeInRight">
                                                    <i class="mdi mdi mdi-delete-forever"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    @endif @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    @endforeach
                </div> --}} {{--
                <ul class="pagination">
                    {{ $messages->links() }}
                </ul> --}} {{--
                <div class="seemore">
                    <button type="button" class="btn btn-link btn-fw">See more messages . . .</button>
                    </dive> --}}
                </div>
            </div>
        </div>
    </div>