var message = new Message();

function Message(){
    
}

Message.prototype.showmessage = function (param, callback) {
	CSRF = $("meta[name='csrf-token']").attr('content');

	data = { 
		_token: CSRF,
		id : param.id
	};

	$.ajax({
		url: '/message/' + param.id + '/show',
		type: 'POST',
		dataType: "json",
		data: data,
		success: function(data){
			if (callback)
				callback(data);
		}, error:function (xhr, error, ajaxOptions, thrownError){
			// util.show500();
			console.log(xhr.responseText);
		}
	});
}
Message.prototype.destroyMessage = function (param, callback) {
	CSRF = $("meta[name='csrf-token']").attr('content');

	data = { 
		_token: CSRF
	};

	$.ajax({
		url: '/message/' + param.id + '/destroy',
		type: 'POST',
		dataType: "json",
		data: data,
		success: function(data){
			if (callback)
				callback(data);
		}, error: function(xhr, error, ajaxOptions, thrownError){
			// util.show500();
			console.log(xhr.responseText);
		}
	});
}