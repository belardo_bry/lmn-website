var blog = new Blog();

function Blog() {

}

Blog.prototype.storeblog = function(param, callback) {
    CSRF = $("meta[name='csrf-token']").attr('content');

    data = {
        _token: CSRF
    };

    $.ajax({
        url: '/blog/store',
        data: new FormData($("#frm_create_blog")[0]),
        type: 'POST',
        dataType: "json",
        processData: false,
        contentType: false,
        success: function(data) {
            if (callback)
                callback(data);
        },
        error: function(xhr, error, ajaxOptions, thrownError) {
            // util.show500();
            console.log(xhr.responseText);
            $('.icon-save').removeClass('mdi-spin').removeClass('mdi-settings').addClass('mdi-file-check');
            $('.btn-save-blog').removeAttr('disabled');
            $('.btn-cancel').removeAttr('disabled');
            $('.image-valid').fadeIn('slow');
        }
    });
}

Blog.prototype.updateblog = function(param, callback) {
    CSRF = $("meta[name='csrf-token']").attr('content');

    data = {
        blog_id: param.blog_id,
        _token: CSRF
    };

    $.ajax({
        url: '/blog/update',
        data: new FormData($("#frm_update_blog")[0]),
        type: 'POST',
        dataType: "json",
        processData: false,
        contentType: false,
        success: function(data) {
            if (callback)
                callback(data);
        },
        error: function(xhr, error, ajaxOptions, thrownError) {
            // util.show500();
            console.log(xhr.responseText);
            $('.icon-save').removeClass('mdi-spin').removeClass('mdi-settings').addClass('mdi-file-check');
            $('.btn-save-blog').removeAttr('disabled');
            $('.btn-cancel').removeAttr('disabled');
            $('.image-valid').fadeIn('slow');
        }
    });
}
Blog.prototype.showblog = function(param, callback) {
    CSRF = $("meta[name='csrf-token']").attr('content');

    data = {
        _token: CSRF,
        title: param.title
    };

    $.ajax({
        url: '/blog/' + param.id + '/show',
        type: 'POST',
        dataType: "json",
        data: data,
        success: function(data) {
            if (callback)
                callback(data);
        }, error: function(xhr, error, ajaxOptions, thrownError) {
            // util.show500();
            console.log(xhr.responseText);
        }
    });
}
Blog.prototype.destroyblog = function(param, callback) {
    CSRF = $("meta[name='csrf-token']").attr('content');

    data = {
        _token: CSRF
    };

    $.ajax({
    url: '/blog/' + param.id + '/destroy',
        type: 'POST',
        dataType: "json",
        data: data,
        success: function(data) {
            if (callback)
                callback(data);
        }, error: function(xhr, error, ajaxOptions, thrownError) {
            // util.show500();
            console.log(xhr.responseText);
        }
    });
}