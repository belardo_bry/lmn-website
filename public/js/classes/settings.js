var settings = new Settings();

function Settings() {

}

Settings.prototype.updateUser = function(param, callback) {
    CSRF = $("meta[name='csrf-token']").attr('content');

    data = {
        _token: CSRF
    };

    $.ajax({
        url: '/user/updateUser',
        data: new FormData($("#frm_update_user")[0]),
        type: 'POST',
        dataType: "json",
        processData: false,
        contentType: false,
        success: function(data) {
            if (callback)
                callback(data);
        },
        error: function(xhr, error, ajaxOptions, thrownError) {
            // util.show500();
            console.log(xhr.responseText);
            $('.icon-save').removeClass('mdi-spin').removeClass('mdi-settings').addClass('mdi-file-check');
            $('.btn-save-blog').removeAttr('disabled');
            $('.btn-cancel').removeAttr('disabled');
            $('.image-valid').fadeIn('slow');
        }
    });
}

Settings.prototype.updateUserPassword = function(param, callback) {
    CSRF = $("meta[name='csrf-token']").attr('content');

    data = {
        _token: CSRF
    };

    $.ajax({
        url: '/user/updateUserPassword',
        data: new FormData($("#frm_update_password")[0]),
        type: 'POST',
        dataType: "json",
        processData: false,
        contentType: false,
        success: function(data) {
            if (callback)
                callback(data);
        },
        error: function(xhr, error, ajaxOptions, thrownError) {
            // util.show500();
            console.log(xhr.responseText);
            $('.icon-save').removeClass('mdi-spin').removeClass('mdi-settings').addClass('mdi-file-check');
            $('.btn-save-blog').removeAttr('disabled');
            $('.btn-cancel').removeAttr('disabled');
            $('.image-valid').fadeIn('slow');
        }
    });
}