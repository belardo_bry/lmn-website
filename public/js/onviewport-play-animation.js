$.fn.extend({
  animateCss: function(animationName, callback) {
    var animationEnd = (function(el) {
      var animations = {
        animation: 'animationend',
        OAnimation: 'oAnimationEnd',
        MozAnimation: 'mozAnimationEnd',
        WebkitAnimation: 'webkitAnimationEnd',
      };

      for (var t in animations) {
        if (el.style[t] !== undefined) {
          return animations[t];
        }
      }
    })(document.createElement('div'));

    this.addClass('animated ' + animationName).one(animationEnd, function() {
      $(this).removeClass('animated ' + animationName);

      if (typeof callback === 'function') callback();
    });

    return this;
  },
});

function onViewport(el, elClass, offset, callback) {
  var didScroll = false;
  var this_top;
  var height;
  var top;

  if(!offset) { var offset = 0; }

  $(window).scroll(function() {
      didScroll = true;
  });

  setInterval(function() {
    if (didScroll) {
      didScroll = false;
      top = $(this).scrollTop();

      $(el).each(function(i){
        this_top = $(this).offset().top - offset;
        height   = $(this).height();

        // Scrolled within current section
        if (top >= this_top && !$(this).hasClass(elClass)) {
          $(this).addClass(elClass);

          if (typeof callback == "function") callback(el);
        }
      });
    }
  }, 100);
}

onViewport(".services-section", "active", 1000, function() {
  $(".lmn-card").addClass("animated fadeInUp");
  $(".our-services").addClass("animated fadeInUp");
  $(".services-content").addClass("animated fadeInUp");
});
onViewport(".info-section", "active", 900, function() {
  $(".who-are-we").addClass("animated fadeInUp");
  $(".who-are-we-img").addClass("animated fadeInUp");
});
onViewport(".team-section", "active", 600, function() {
  $(".team-header").addClass("animated fadeInUp");
  $(".team-content").addClass("animated fadeInUp");
});
onViewport(".contact-section", "active", 700, function() {
  $(".contact-fadeInUp").addClass("animated fadeInUp");
});
onViewport(".map-location", "active", 600, function() {
  $(".mbr-map").addClass("animated fadeInUp");
});
// onViewport(".copyright-footer", "active", 500, function() {
//   $(".copyright-text").addClass("animated fadeInUp");
// });
// end