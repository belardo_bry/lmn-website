// $('.slimscroll').slimScroll({
// 	height: '400px'
// });

// $('.action-dropdown').click(function(){
// 	var id = $(this).attr('id');
// });

// $("tbody tr").click(function(){
// 	var rowid = $(this).attr('id');
// 	// alert("selected id is " + rowid);
// 	var selected = parseInt($('.hdn-selected-row').val());

// 	if ($(this).hasClass('selected')){
// 		$(this).removeClass('selected');

// 		var total = selected - 1;
// 		$('.hdn-selected-row').val(total);
// 		// alert(total);
// 		$("#btn-delete-all").text("Delete all" + " ("+ total +")");
// 		if (total == 0){
// 			$("#btn-delete-all").removeClass('btn-delete-all-selected');
// 			$("#btn-delete-all").addClass('btn-delete-all');
// 			$("#btn-delete-all").text("Delete all");
// 		}
// 	}else {
// 		$(this).addClass('selected');

// 		var total = selected + 1;
// 		$('.hdn-selected-row').val(total);
// 		// alert(total);
// 		$("#btn-delete-all").text("Delete all" + " ("+ total +")");

// 		if ($("#btn-delete-all").hasClass('btn-delete-all')) {
// 			$("#btn-delete-all").removeClass('btn-delete-all');
// 			$("#btn-delete-all").addClass('btn-delete-all-selected');
// 		}
// 	}
// });
function success(event) {
    $.notify({ // options
        icon: 'mdi mdi-thumb-up-outline',
        title: '<strong> Successful!</strong>',
        message: event.message,
        target: '_blank'
    }, {
        // settings
        element: 'body',
        position: null,
        type: "success",
        allow_dismiss: true,
        newest_on_top: false,
        showProgressbar: false,
        placement: {
            from: "top",
            align: "right"
        },
        offset: 20,
        spacing: 10,
        z_index: 1031,
        delay: 3000,
        timer: 1000,
        url_target: '_blank',
        mouse_over: null,
        animate: {
            enter: 'animated fadeInDown',
            exit: 'animated fadeOutUp'
        },
        onShow: null,
        onShown: null,
        onClose: null,
        onClosed: null,
        icon_type: 'class',
        template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
            '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
            '<span data-notify="icon"></span> ' +
            '<span data-notify="title">{1}</span> ' +
            '<span data-notify="message">{2}</span>' +
            '<div class="progress" data-notify="progressbar">' +
            '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
            '</div>' +
            '<a href="{3}" target="{4}" data-notify="url"></a>' +
            '</div>'
    });
}

function error(event) {
    $.notify({ // options
        icon: 'mdi mdi-alert-circle-outline',
        title: '<strong> Error!</strong>',
        message: event.message,
        target: '_blank'
    }, {
        // settings
        element: 'body',
        position: null,
        type: "danger",
        allow_dismiss: true,
        newest_on_top: false,
        showProgressbar: false,
        placement: {
            from: "top",
            align: "right"
        },
        offset: 20,
        spacing: 10,
        z_index: 1031,
        delay: 3000,
        timer: 1000,
        url_target: '_blank',
        mouse_over: null,
        animate: {
            enter: 'animated fadeInDown',
            exit: 'animated fadeOutUp'
        },
        onShow: null,
        onShown: null,
        onClose: null,
        onClosed: null,
        icon_type: 'class',
        template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
            '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
            '<span data-notify="icon"></span> ' +
            '<span data-notify="title">{1}</span> ' +
            '<span data-notify="message">{2}</span>' +
            '<div class="progress" data-notify="progressbar">' +
            '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
            '</div>' +
            '<a href="{3}" target="{4}" data-notify="url"></a>' +
            '</div>'
    });
}

$('.select-all').click(function() {
    $("tbody tr").addClass('selected');
    if ($("tbody tr").hasClass('selected')) {
        alert($('.selected').lenght);
    }
});

$('.btn-view-message').click(function() {
    var message_id = $(this).attr('id');
    // console.log(message_id);

    var post_obj = {
        id: message_id,
        'path': window.location.pathname
    };
    message.showmessage(post_obj, function(data) {
        if (data.status == 'OK') {
            // console.log(data.message.name, data.message.created_at);
            $('.sender_name').text(data.message.name);
            $('.sender_email').text(data.message.email);
            $('.sender_number').text(data.message.mobile_number);
            $('.sender_date_time').text(data.message.created_at);
            $('.sender_message').text(data.message.message);
            $('#view_message').modal('show');
        } else {
            console.log(data.error);
        }
    });
});

$('.btn-delete').click(function() {
    var message_id = $(this).attr('id');
    $('.btn-yes-delete').attr('id', message_id);
    $('.msg-qsn').text('Are you sure you want to delete this message?');
    $('#delete_message').modal('show');
});

$('.btn-yes-delete').click(function() {
    var message_id = $(this).attr('id');

    $('.icon-delete').addClass('mdi-spin').addClass('mdi-settings').removeClass('mdi-delete-forever');
    $('.btn-yes-delete').attr('disabled', 'disabled');
    $('.btn-cancel').attr('disabled', 'disabled');

    setTimeout(function() {
        var post_obj = {
            id: message_id,
            'path': window.location.pathname
        };
        message.destroyMessage(post_obj, function(data) {
            if (data.status == 'OK') {
                $('.icon-delete').removeClass('mdi-spin').removeClass('mdi-settings').addClass('mdi-delete-forever');
                $('.btn-yes-delete').removeAttr('disabled');
                $('.btn-cancel').removeAttr('disabled');
                $('#delete_message').modal('hide');
                $('#row' + message_id).fadeOut();
                $('#delete_message').modal('hide');
                var msgStatus = {
                    message: "you successfully deleted message."
                };
                success(msgStatus);
            } else {
                console.log(data.error);
            }
        });
    }, 2000);
});