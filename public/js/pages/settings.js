
$('#frm_update_user').on('submit', function(event) {

    $('.btn_update_user').attr('disabled', 'disabled').find('i').addClass('mdi-settings mdi-spin').removeClass('mdi-file-check');

    var post_obj = {
        'path': window.location.pathname
    };
    settings.updateUser(post_obj, function(data) {
        if (data.status == "OK") {
            // $('.btn_update_user').attr('disabled', 'disabled').find('i').removeClass('mdi-spin').removeClass('mdi-settings').addClass('mdi-file-check');
            location.reload(true);
        } else if(data.status == "ERROR"){
            console.log(data.error);
            $('.btn_update_user').attr('disabled', 'disabled').find('i').removeClass('mdi-spin').removeClass('mdi-settings').addClass('mdi-file-check');
        } else {
            console.log(error);
            $('.btn_update_user').attr('disabled', 'disabled').find('i').removeClass('mdi-spin').removeClass('mdi-settings').addClass('mdi-file-check');
        }
    });
});

$('#frm_update_password').on('submit', function(event) {

    $('.btn_update_user_password').attr('disabled', 'disabled').find('i').addClass('mdi-settings mdi-spin').removeClass('mdi-file-check');

    var newpass = $('#newpassword').val();
    var reenterPass = $('#reenternewPassword').val();

    if (reenterPass != newpass) {
        $('#reenternewPassword').addClass('is-invalid');
        $('.btn_update_user_password').attr('disabled', 'disabled').find('i').removeClass('mdi-spin').removeClass('mdi-settings').addClass('mdi-file-check');
    }else{
        var post_obj = {
            'path': window.location.pathname
        };
        settings.updateUser(post_obj, function(data) {
            if (data.status == "OK") {
                // $('.btn_update_user_password').attr('disabled', 'disabled').find('i').removeClass('mdi-spin').removeClass('mdi-settings').addClass('mdi-file-check');
                location.reload(true);
            } else if(data.status == "ERROR"){
                console.log(data.error);
                $('.btn_update_user_password').attr('disabled', 'disabled').find('i').removeClass('mdi-spin').removeClass('mdi-settings').addClass('mdi-file-check');
            } else {
                console.log(error);
                $('.btn_update_user_password').attr('disabled', 'disabled').find('i').removeClass('mdi-spin').removeClass('mdi-settings').addClass('mdi-file-check');
            }
        });
    }
});
