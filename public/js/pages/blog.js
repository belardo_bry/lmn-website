
// function ajaxLoad(filename, content) {
//     content = typeof content !== 'undefined' ? content : 'content';
//     $('.loading_page').show();
//     $('.page_content').hide();
//     $.ajax({
//         type: "GET",
//         url: filename,
//         contentType: false,
//         success: function (data) {
//             $('#' + content).html(data);
//             $('.loading_page').hide();
//             $('.page_content').show();
//         },
//         error: function (xhr, status, error) {
//             alert(xhr, responseText);
//         }
//     });
// }


$(document).ready(function(){
    var img = $('#uploadedImage');
    var iconUpload = $('.icon');
    var upload = $('#img_blog');
    var image_upload = $('.drop-upload-area');
    var image_area =$('.image_area');
    // $('#select_image').hide();

    $("#frm_create_blog")[0].reset();
    $('.image_area').css('display', 'none');
    $('.drop-upload-area').show();
    $('#blogTitle').val('');
    $('.image_area').hide();
    $('.image-valid').fadeOut('fast');

    var appenText = $.map($(".cntb"), function(n, i){
      return n.id;
    });
    // alert(arrayOfIds);
    $.each( appenText, function( key, value ) {
        var blogId = key + 1;

        var blog_content = $('#blog-content' + blogId);
        var append_content = $('#' + value).val();
        blog_content.append(append_content);
    });

    $('#img_blog').on('change', function(e) {
        var files = e.target.files;
        var reader = new FileReader();

        reader.onload = function (event) {
            img.attr('src',event.target.result);
        }
        reader.readAsDataURL(e.target.files[0]);
        // hdnimgname.val(files[0].name);
        // img.attr('src', URL.createObjectURL(files[0]));
        image_upload.hide();
        img.fadeIn();
        image_area.css('border', '1px solid #f5f3f3').fadeIn();
        upload.removeClass('dragarea').addClass('photodrag');
    });

    $('#new_image').on('change', function(e) {
        var files = e.target.files;
        var reader = new FileReader();

        reader.onload = function (event) {
            $('#update_image').attr('src',event.target.result);
        }
        reader.readAsDataURL(e.target.files[0]);
        $('#hdnimagename').val(files[0].name);
    });
    // $('.image_are').css('color','#fff');

});

$(document).ready(function(){
    tinymce.init({
        selector: '#updateContent',
        body_class: 'blog_update_editor',
        paste_as_text: true,
        branding: false,
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste",
            "emoticons",
            "textcolor colorpicker",
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | emoticons | forecolor backcolor",
        height: 200
    });
});

$('.create_blog').click('click', function(){
    tinymce.init({
        selector: '#blogContent',
        body_class: 'blog_editor',
        paste_as_text: true,
        branding: false,
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste",
            "emoticons",
            "textcolor colorpicker",
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | emoticons | forecolor backcolor",
        height: 200
    });
});

function clear() {
    $("#frm_create_blog")[0].reset();
    $('.image_area').css('display', 'none');
    $('.drop-upload-area').show();
    $('#blogTitle').val('');
    $('.image_area').hide();
    $('.image-valid').fadeOut('fast');
    tinyMCE.activeEditor.setContent('');
}

function success(event) {
    $.notify({ // options
        icon: 'mdi mdi-thumb-up-outline',
        title: '<strong> Successful!</strong>',
        message: event.message,
        target: '_blank'
    }, {
        // settings
        element: 'body',
        position: null,
        type: "success",
        allow_dismiss: true,
        newest_on_top: false,
        showProgressbar: false,
        placement: {
            from: "top",
            align: "right"
        },
        offset: 20,
        spacing: 10,
        z_index: 1031,
        delay: 3000,
        timer: 1000,
        url_target: '_blank',
        mouse_over: null,
        animate: {
            enter: 'animated fadeInDown',
            exit: 'animated fadeOutUp'
        },
        onShow: null,
        onShown: null,
        onClose: null,
        onClosed: null,
        icon_type: 'class',
        template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
            '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
            '<span data-notify="icon"></span> ' +
            '<span data-notify="title">{1}</span> ' +
            '<span data-notify="message">{2}</span>' +
            '<div class="progress" data-notify="progressbar">' +
            '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
            '</div>' +
            '<a href="{3}" target="{4}" data-notify="url"></a>' +
            '</div>'
    });
}

function error(event) {
    $.notify({ // options
        icon: 'mdi mdi-alert-circle-outline',
        title: '<strong> Error!</strong>',
        message: event.message,
        target: '_blank'
    }, {
        // settings
        element: 'body',
        position: null,
        type: "danger",
        allow_dismiss: true,
        newest_on_top: false,
        showProgressbar: false,
        placement: {
            from: "top",
            align: "right"
        },
        offset: 20,
        spacing: 10,
        z_index: 1031,
        delay: 3000,
        timer: 1000,
        url_target: '_blank',
        mouse_over: null,
        animate: {
            enter: 'animated fadeInDown',
            exit: 'animated fadeOutUp'
        },
        onShow: null,
        onShown: null,
        onClose: null,
        onClosed: null,
        icon_type: 'class',
        template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
            '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
            '<span data-notify="icon"></span> ' +
            '<span data-notify="title">{1}</span> ' +
            '<span data-notify="message">{2}</span>' +
            '<div class="progress" data-notify="progressbar">' +
            '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
            '</div>' +
            '<a href="{3}" target="{4}" data-notify="url"></a>' +
            '</div>'
    });
}

$('.close_modal').click(function(){
    clear();
});

$("#img_blog").mouseover(function() {
    $('.icon').css("display","block");
    $('.image_area a').css("background","rgba(255, 255, 255, .5)");
}).mouseout(function() {
    $('.icon').css("display","none");
    $('.image_area a').css("background","none");
});;

(function($) {
    $.fn.checkFileType = function(options) {
        var defaults = {
            allowedExtensions: [],
            success: function() {},
            error: function() {}
        };
        options = $.extend(defaults, options);

        return this.each(function() {
            $(this).on('change', function() {
                var value = $(this).val(),
                    file = value.toLowerCase(),
                    extension = file.substring(file.lastIndexOf('.') + 1);

                if ($.inArray(extension, options.allowedExtensions) == -1) {
                    options.error();
                    $(this).focus();
                } else {
                    options.success();
                }
            });
        });
    };
})(jQuery);

$(function() {
    $('#img_blog').checkFileType({
        allowedExtensions: ['jpg', 'jpeg', 'png', 'gif'],
        success: function() {
            console.log('Success.');
            $('.image-valid').fadeOut('slow');
        },
        error: function() {
            $('#img_blog').val('');
            $('.image_area').hide();
            $('.image-valid').fadeIn('slow');
            $(".drop-upload-area").css('border', '3px dashed #fe7c96').removeClass('text-primary').addClass('text-danger').show();
        }
    });
});

// $('.btn-save-blog').click('click', function(e){
$('#frm_create_blog').on('submit', function(event) {
    event.preventDefault();
    var title = $('.title-valid');
    var content_valid = $('.content-valid');
    var image = $('.image-valid');
    var content = tinyMCE.activeEditor.getContent();

    $('.icon-save').addClass('mdi-spin').addClass('mdi-settings').removeClass('mdi-file-check');
    $('.btn-save-blog').attr('disabled', 'disabled');
    $('.btn-cancel').attr('disabled', 'disabled');
    setTimeout(function() {
        if (content === "" || content === null) {
            content_valid.fadeIn('slow');
            $('.icon-save').removeClass('mdi-spin').removeClass('mdi-settings').addClass('mdi-file-check');
            $('.btn-save-blog').removeAttr('disabled');
            $('.btn-cancel').removeAttr('disabled');
        }else if ($('#blogTitle').val() === "") {
            title.fadeIn('slow');
            $('.icon-save').removeClass('mdi-spin').removeClass('mdi-settings').addClass('mdi-file-check');
            $('.btn-save-blog').removeAttr('disabled');
            $('.btn-cancel').removeAttr('disabled');
        }else {
            var post_obj = {
                'path': window.location.pathname
            };
            blog.storeblog(post_obj, function(data) {
                if (data.status == "OK") {
                    $('#btn_back').click();
                    $('.icon-save').removeClass('mdi-spin').removeClass('mdi-settings').addClass('mdi-file-check');
                    $('.btn-save-blog').removeAttr('disabled');
                    $('.btn-cancel').removeAttr('disabled');
                    $('#create_blog').modal('hide');
                    $('.modal-backdrop').remove();

                    var insert = $(
                                '<div class="col-lg-4 col-sm-6 blog-item" id="blog'+ data.blog.id +'">'
                        +            '<div class="card h-100 blog-card">'
                        +                '<div class="blog-image">'
                        +                    '<img src="/img/blog/post/'+ data.blog.image +'" alt="Image" style="">'
                        +                    '<div class="ovrly"></div>'
                        +                     '<div id="blog_item_loader_'+ data.blog.id +'" class="blog_item_loader">'
                        +                        '<div class="preloader">'
                        +                           '<div class="spinner-layer pl-white">'
                        +                                '<div class="circle-clipper left">'
                        +                                    '<div class="circle"></div>'
                        +                                '</div>'
                        +                                '<div class="circle-clipper right">'
                        +                                    '<div class="circle"></div>'
                        +                                '</div>'
                        +                            '</div>'
                        +                        '</div>'
                        +                    '</div>'
                        +                    '<div class="buttons">'
                        +                        '<a href="#delete_message" data-toggle="modal" class="btn btn-gradient-danger btn-rounded btn-icon animated fadeInUp btn-action btn-delete" data-id="'+ data.blog.id +'">'
                        +                            '<i class="mdi mdi-delete-forever"></i>'
                        +                        '</a>'
                        +                        '<a href="#{{ $result->title }}" class="btn btn-gradient-success btn-rounded btn-icon animated fadeInUp btn-action btn-edit" data-id="'+ data.blog.id +'">'
                        +                            '<i class="mdi mdi-pencil"></i>'
                        +                        '</a>'
                        +                        '<a href="#" data-toggle="modal" class="btn btn-gradient-primary btn-rounded btn-icon animated fadeInUp btn-action btn-view" data-id="'+ data.blog.id +'">'
                        +                            '<i class="mdi mdi-eye"></i>'
                        +                        '</a>'
                        +                    '</div>'
                        +                '</div>'
                        +                '<div class="blog-title">'
                        +                    '<h5>'+ data.blog.title +'</h5>'
                        +                    '<div class="d-flex flex-row align-items-center text-gray">'
                        +                        '<i class="mdi mdi-calendar icon-sm"></i>'
                        +                        '<p class="mb-0 ml-1">'
                        +                          '<small>September 27, 1998</small>'
                        +                        '</p>'
                        +                    '</div>'
                        +                    '<div class="d-flex flex-row align-items-center text-gray">'
                        +                        '<i class="mdi mdi-clock icon-sm"></i>'
                        +                        '<p class="mb-0 ml-1">'
                        +                            '<small>09:00 PM</small>'
                        +                        '</p>'
                        +                    '</div>'
                        +                '</div>'
                        +            '</div>'
                        +        '</div>'
                        );

                    // ajaxLoad('/admin/dashboard/blog');

                    insert.insertAfter('.new_blog');
                    content_valid.css('display','none');
                    title.css('display','none');
                    image.css('display','none');
                    var msgStatus = {
                        message: "you successfully created blog."
                    };
                    clear();
                    success(msgStatus);

                } else {
                    console.log(data.error);
                    $('.icon-save').removeClass('mdi-spin').removeClass('mdi-settings').addClass('mdi-file-check');
                    $('.btn-save-blog').removeAttr('disabled');
                    $('.btn-cancel').removeAttr('disabled');
                    $('#create_blog').modal('hide');
                    var msgStatus = {
                        message: "image is required."
                    };
                    clear();
                    error(msgStatus);
                }
            });
        }
    }, 2000);
});

$('.btn-cancel').click(function(){
    clear();
});

$(document).on('click', '.btn-delete', function (){
    var blog_id = $(this).data('id');
    $('#hdn-data-id').val(blog_id);
    $('.msg-qsn').text('Are you sure you want to delete this blog?');
});

$('.btn-delete').unbind('click').on('click', function () {
    var blog_id = $(this).data('id');
    $('#hdn-data-id').val(blog_id);
    $('.msg-qsn').text('Are you sure you want to delete this blog?');
});

$('.btn-yes-delete').unbind('click').on('click', function() {
    var id = $('#hdn-data-id').val();

    $('.icon-delete').addClass('mdi-spin').addClass('mdi-settings').removeClass('mdi-file-check');
    $(this).attr('disabled', 'disabled');
    $('.btn-cancel').attr('disabled', 'disabled');

    setTimeout(function() {
        var post_obj = {
            id: id,
            'path': window.location.pathname
        };

        blog.destroyblog(post_obj, function(data) {
            if (data.status == 'OK') {
                $('.icon-delete').removeClass('mdi-spin').removeClass('mdi-settings').addClass('mdi-file-check');
                $('.btn-yes-delete').removeAttr('disabled');
                $('.btn-cancel').removeAttr('disabled');
                $('#blog' + id).fadeOut();
                $('#delete_message').modal('hide');
                $('.modal-backdrop').remove();
                var msgStatus = {
                    message: "You permanently delete a blog."
                };
                success(msgStatus);
            } else {
                console.log(data.error);
                alert(data.error);
            }
        });
    }, 2000);
});

$('#back_to_blogs').click(function(){
    setTimeout(function() {
        $('#blog_details').css('display', 'none');
        $('#page_header').css('display', 'block');
        $('#all_blogs').css('display', 'block');
        $('.details').text("");
    }, 1000);
});

$('#update_back_to_blogs').click(function(){
    $('#update_blog').css('display', 'none');
    $('#all_blogs').css('display', 'block');
    tinyMCE.activeEditor.setContent('');
});

$('#btn_cancel_blog').click(function(){
    $('#update_blog').css('display', 'none');
    $('#all_blogs').css('display', 'block');
    tinyMCE.activeEditor.setContent('');
});

$('#btn_back').click(function(){
    setTimeout(function() {
        $('#blog_details').css('display', 'none');
        $('#page_header').css('display', 'block');
        $('#all_blogs').css('display', 'block');
        $('.details').text("");
    }, 1000);
});

$(document).on('click', '.btn-view', function (){
    var id = $(this).data('id');
    var title = $('.txt_title_'+id).text();

    $('#blog_item_loader_' + id).show();
    $('.item_loading_' + id).addClass('loading-item');
    $('.buttons').addClass('hide-buttons');
    var post_obj = {
        id: id,
        title: title,
        'path': window.location.pathname
    };

    blog.showblog(post_obj, function(data) {
        if (data.status == 'OK') {
            var blogTitle = $('.txt_title_blog');
            var blogDate = $('.txt_date');
            var blogTime = $('.txt_time');
            var blogImage = $('.image_blog img');
            var blogDetails = $('.details');
            var blogtitlespan = $('.blog-title-span');
            setTimeout(function() {
                $('#blog_details').css('display', 'block');
                $('#page_header').css('display', 'none');
                $('#all_blogs').css('display', 'none');
                $('#blog_item_loader_' + id).hide();
                $('.item_loading_' + id).removeClass('loading-item');
                $('.buttons').removeClass('hide-buttons');

                blogTitle.text(data.blog.title);
                blogtitlespan.text(data.blog.title);
                blogDate.text($('.date_created_'+data.blog.id).text());
                blogTime.text($('.time_created_'+data.blog.id).text());
                blogImage.attr('src', '/img/blog/post/'+data.blog.image);
                blogDetails.append(data.blog.content);
            }, 2000);
        } else {
            console.log(data.error);
            alert(data.error);
        }
    });
});

// $('.btn-view').unbind('click').on('click', function () {
//     var id = $(this).data('id');
//     var title = $('.txt_title_'+id).text();

//     $('#blog_item_loader_' + id).show();
//     $('.item_loading_' + id).addClass('loading-item');
//     $('.buttons').addClass('hide-buttons');
//     var post_obj = {
//         id: id,
//         title: title,
//         'path': window.location.pathname
//     };

//     blog.showblog(post_obj, function(data) {
//         if (data.status == 'OK') {
//             var blogTitle = $('.txt_title_blog');
//             var blogDate = $('.txt_date');
//             var blogTime = $('.txt_time');
//             var blogImage = $('.image_blog img');
//             var blogDetails = $('.details');
//             var blogtitlespan = $('.blog-title-span');
//             setTimeout(function() {
//                 $('#blog_details').css('display', 'block');
//                 $('#page_header').css('display', 'none');
//                 $('#all_blogs').css('display', 'none');
//                 $('#blog_item_loader_' + id).hide();
//                 $('.item_loading_' + id).removeClass('loading-item');
//                 $('.buttons').removeClass('hide-buttons');

//                 blogTitle.text(data.blog.title);
//                 blogtitlespan.text(data.blog.title);
//                 blogImage.attr('src', '/img/blog/post/'+data.blog.image);
//                 blogDetails.append(data.blog.content);
//             }, 2000);
//         } else {
//             console.log(data.error);
//             alert(data.error);
//         }
//     });
// });

$('.btn-edit').click(function(){
    var id = $(this).data('id');
    var title = $('.txt_title_'+id).text();

    $('#update_blog').show();
    $('#all_blogs').hide();

    var post_obj = {
        id: id,
        title: title,
        'path': window.location.pathname
    };

    blog.showblog(post_obj, function(data) {
        if (data.status == 'OK') {
            $('#Update_title').val(data.blog.title);
            $('#update_image').attr('src', '/img/blog/post/'+data.blog.image);
            $('#hdnimagename').val(data.blog.image);
            var $body = $(tinymce.activeEditor.getBody());
            $body.append($(data.blog.content));
            $('.btn_update_blog').attr('data-id', data.blog.id);
            $('#hdnID').val(data.blog.id);
        } else {
            console.log(data.error);
            alert(data.error);
        }
    });

});

$('#frm_update_blog').on('submit', function(event) {
    var id = $('.btn_update_blog').attr('data-id');
    var title = $('#Update_title').val();
    var image = $('#new_image');
    console.log(id);

    $('.btn_update_blog').attr('disabled', 'disabled').find('i').addClass('mdi-settings  mdi-spin').removeClass('mdi-file-check');
    var post_obj = {
        blog_id: id
    };
    blog.updateblog(post_obj, function(data) {
        if (data.status == "OK") {
            $('.btn_update_blog').attr('disabled', 'disabled').find('i').removeClass('mdi-spin').removeClass('mdi-settings').addClass('mdi-file-check');
            location.reload(true);
        } else if(data.status == "ERROR"){
            console.log(data.error);
            $('.btn_update_blog').attr('disabled', 'disabled').find('i').removeClass('mdi-spin').removeClass('mdi-settings').addClass('mdi-file-check');
        } else {
            console.log(error);
            $('.btn_update_blog').attr('disabled', 'disabled').find('i').removeClass('mdi-spin').removeClass('mdi-settings').addClass('mdi-file-check');
        }
    });
});

$('.btn_new_image').click(function(){
    $('#new_image').click();
});