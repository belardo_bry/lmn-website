var messages = new Messages();

function Messages(){

}

Messages.prototype.bindMessages = function () {
    $('input[name=name]').val("");
    $('input[name=email]').val("");
    $('input[name=mobile_number]').val("");
    $('#subject').val('Game Development');
    $('#message').val("");

    $('.btn-send-message').text("Send Message");
    $('.btn-send-message').removeAttr("disabled");
}

$("#frm-add-message").unbind('submit').on('submit', function (event) {
    event.preventDefault();
    var token = $('input[name=_token]').val();
    var name = $('input[name=name]').val();
    var email = $('input[name=email]').val();
    var mobile_number = $('input[name=mobile_number]').val();
    var subject = $('#subject').val();
    var message = $('#message').val();
    $('.btn-send-message').text("Sending . . .");
    $('.btn-send-message').attr("disabled", "disabled");

    data = {
        _token: token,
        'name' : name,
        'email' : email,
        'mobile_number' : mobile_number,
        'subject' : subject,
        'message' : message,
        'path' : window.location.pathname
    };

    $.ajax({
        url: '/messages/ajaxStore',
        type: 'POST',
        dataType: "json",
        data: data,
        success: function(data){
            $.notify({  // options
                icon: 'mdi mdi-thumb-up-outline',
                title: '<strong> Sucessfuly!</strong>',
                message: 'Message has sent.',
                target: '_blank'
            },{
                // settings
                element: 'body',
                position: null,
                type: "success",
                allow_dismiss: true,
                newest_on_top: false,
                showProgressbar: false,
                placement: {
                    from: "top",
                    align: "right"
                },
                offset: 20,
                spacing: 10,
                z_index: 1031,
                delay: 3000,
                timer: 1000,
                url_target: '_blank',
                mouse_over: null,
                animate: {
                    enter: 'animated fadeInDown',
                    exit: 'animated fadeOutUp'
                },
                onShow: null,
                onShown: null,
                onClose: null,
                onClosed: null,
                icon_type: 'class',
                template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
                                '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
                                '<span data-notify="icon"></span> ' +
                                '<span data-notify="title">{1}</span> ' +
                                '<span data-notify="message">{2}</span>' +
                                '<div class="progress" data-notify="progressbar">' +
                                    '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                                '</div>' +
                                '<a href="{3}" target="{4}" data-notify="url"></a>' +
                            '</div>' 
            });
            messages.bindMessages();
        }, error:function (xhr, error, ajaxOptions, thrownError){
            console.log(xhr.responseText);
        }
    });
});
