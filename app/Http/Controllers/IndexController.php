<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subject;
use App\Blog;
use App\Message;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subjects = Subject::all();
        return view('index', compact('subjects'))->with(array('page' => 'index'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'mobile_number' => 'required',
            'subject' => 'required',
            'message' => 'required'
        ]);

        $message = new Message;
        $message->name = $request->name;
        $message->email = $request->email;
        $message->mobile_number = $request->mobile_number;
        $message->subject = $request->subject;
        $message->message = $request->message;
        $message->save();

        return array('status' => 'OK', 'message' => $message);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



    public function blogs()
    {
        $blogs = Blog::orderBy('id', 'desc')->get();
        return view('blog/blogs', compact('blogs'))->with(array('page' => 'blogs'));
    }

    public function blogShow(Request $request, $id)
    {
        $blog = Blog::find($id);
        if (!$blog)
            return array('status' => 'ERROR', 'error' => 'Oops. Something went wrong. Blog not found.');

        return view('blog/blogs')->with(array('page' => 'blog', 'blog' => $blog));
    }
}
