<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;
use App\User;
use App\http\Requests;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function settings(Request $request)
    {
        return view('admin/admin')->with(array('page' => 'admin_settings'));
    }

    public function updateUser(Request $request)
    {
        $user_id = Auth::user()->id;

        $user = User::find($user_id);
        $user->name = $request->displayname;
        $user->email = $request->email;
        if (!$user->save()) {
            return array('status' => 'ERROR', 'error' => 'notification not save.');
        }else{
            return array('status' => 'OK', 'data' => $user);
        }
    }

    public function updateUserPassword(Request $request)
    {
        $user_id = Auth::user()->id;
        $user = User::find($user_id);
        if (!$user)
            return array('status' => 'ERROR', 'error' => 'Oops. Something went wrong. User not found.');

        if (Hash::check($request->password, $user->password)){
            return array('status' => 'ERROR', 'error' => 'You currently use this password.');
        }else{
            $user->password = Hash::make($request->password);
            $user->save();
            return array('status' => 'OK', 'data' => $user);
        }
    }

}
