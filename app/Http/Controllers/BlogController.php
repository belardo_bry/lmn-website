<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Blog;
use Storage;
use File;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $blogs = Blog::orderBy('id', 'desc')->get();
        return view('admin.admin', compact('blogs'))->with(array('page' => 'admin_blog'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $title = Input::get('blogTitle');
        $content = Input::get('blogContent');

        $this->validate($request, [
            'img_blog' => 'required|image|mimes:jpeg,png,jpg'
        ]);
        // dd($request->all());

        // $request->img_blog->store('BlogImages');
        // $imageName = $request->img_blog->store('BlogImages');

        $imageName = hash('sha256', time()).'.'.$request->img_blog->getClientOriginalExtension();
        $distination = public_path('/img/blog/post');
        $request->img_blog->move($distination, $imageName);

        $blog = new Blog;
        $blog->title = $title;
        $blog->content = $content;
        $blog->image = $imageName;
        $blog->save();

        return array('status' => 'OK', 'blog' => $blog);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $blog = Blog::find($id);
        if (!$blog)
            return array('status' => 'ERROR', 'error' => 'Oops. Something went wrong. Post not found.');

        return array('status' => 'OK', 'blog' => $blog);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $title = $request->Update_title;
        $content = $request->updateContent;

        if ($request->new_image != null) {
            $this->validate($request, [
                'new_image' => 'required|image|mimes:jpeg,png,jpg'
            ]);

            $imageName = hash('sha256', time()).'.'.$request->new_image->getClientOriginalExtension();
            $distination = public_path('/img/blog/post');
            $request->new_image->move($distination, $imageName);

            $blog = Blog::find($request->hdnID);
            if (!$blog)
                return array('status' => 'ERROR', 'error' => 'Oops. Something went wrong. Blog not found.');
            $blog->title = $title;
            $blog->content = $content;
            $blog->image = $imageName;
            $blog->save();
        }else{
            $blog = Blog::find($request->hdnID);
            if (!$blog)
                return array('status' => 'ERROR', 'error' => 'Oops. Something went wrong. Blog not found.');
            $blog->title = $title;
            $blog->content = $content;
            $blog->save();
        }

        

        return array('status' => 'OK', 'blog' => $blog);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $blog = Blog::find($id);

        if (!$blog)
            return array('status' => 'ERROR', 'error' => 'Oops. Something went wrong. Blog not found.');

        $image_path = public_path('/img/blog/post/'.$blog->image);
        unlink($image_path);

        $blog->delete();

        return array('status' => 'OK');
    }

}
