<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Message;
use App\Subject;
use App\http\Requests;

class messagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function messages(Request $request)
    {

        // $messages = new Message();
        // $messages = $messages->where('name', 'like', '%')
        //                 ->orderBy($request->session()->get('field'), $request->session()->get('sort'))
        //                 ->paginate(5);

        // $messages = Message::orderBy('id', 'desc')->paginate(5);
        $subjects = Subject::all();
        $messages = Message::orderBy('id', 'desc')->get();
        return view('admin.admin', compact('messages', 'subjects'))->with(array('page' => 'admin_message'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $messages = Message::find($id);
        if (!$messages)
            return array('status' => 'ERROR', 'error' => 'Oops. Something went wrong. Message not found.');

        return array('status' => 'OK', 'message' => $messages);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $messages = Message::find($id);

        if (!$messages)
            return array('status' => 'ERROR', 'error' => 'Oops. Something went wrong. Message not found.');

        $messages->delete();        

        return array('status' => 'OK');
    }
}
