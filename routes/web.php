<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/hdnadmin', 'Auth\LoginController@showLoginForm')->name('admin.login');

Route::get('/', 'IndexController@index');
Route::get('/blogs', 'IndexController@blogs');

Route::get('/pricing', function () {
    return view('pricing/pricing')->with(array('page' => 'pricing'));
});

Route::get('/blog/{id}', 'IndexController@blogShow');
Route::post('messages/ajaxStore', 'IndexController@store');

Route::get('admin/dashboard/blog', 'BlogController@index');
Route::post('blog/store', 'BlogController@store');
Route::post('blog/{blog}/destroy', 'BlogController@destroy');
Route::post('blog/{blog}/show', 'BlogController@show');
Route::post('/blog/update', 'BlogController@update');

// Route::match(['get', 'post'], 'admin/dashboard/messages', 'MessagesController@messages');
Route::get('admin/dashboard/messages', 'MessagesController@messages');
Route::post('message/{comment}/show', 'MessagesController@show');
Route::post('message/{comment}/destroy', 'MessagesController@destroy');

// settings
Route::post('/user/updateUser', 'SettingsController@updateUser');
Route::post('/user/updateUserPassword', 'SettingsController@updateUserPassword');
Route::get('/admin/dashboard/settings', 'SettingsController@settings');



// laravel command

//Clear Cache facade value:
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    return '<h1>Cache facade value cleared</h1>';
});

//Reoptimized class loader:
Route::get('/optimize', function() {
    $exitCode = Artisan::call('optimize');
    return '<h1>Reoptimized class loader</h1>';
});

//Route cache:
Route::get('/route-cache', function() {
    $exitCode = Artisan::call('route:cache');
    return '<h1>Routes cached</h1>';
});

//Clear Route cache:
Route::get('/route-clear', function() {
    $exitCode = Artisan::call('route:clear');
    return '<h1>Route cache cleared</h1>';
});

//Clear View cache:
Route::get('/view-clear', function() {
    $exitCode = Artisan::call('view:clear');
    return '<h1>View cache cleared</h1>';
});

//Clear Config cache:
Route::get('/config-cache', function() {
    $exitCode = Artisan::call('config:cache');
    return '<h1>Clear Config cleared</h1>';
});

//Clear Config cache:
Route::get('/key-generate', function() {
    $exitCode = Artisan::call('key:generate');
    return '<h1>Generate key: '+ $exitCode +'</h1>';
});