<?php

use Illuminate\Database\Seeder;
use App\Subject;

class SubjectsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $GD = Subject::create([
        	'subjects'=>'Game Development',
        ]);
        $WD = Subject::create([
        	'subjects'=>'Web Development',
        ]);
        $MSD = Subject::create([
        	'subjects'=>'Mobile & Software Development',
        ]);
        $SMIM = Subject::create([
        	'subjects'=>'Social Media/Internet Marketing',
        ]);
        $GDA = Subject::create([
        	'subjects'=>'Graphic Design & Advertising',
        ]);
        $ITIC = Subject::create([
        	'subjects'=>'IT Training & IT Consultation',
        ]);
    }
}
